# Tripperty Server Manual

## Operating system

Although any UNIX-based operating system will match server requirements, we recommend to use the same OS we used for tests : **CentOS 7**.

### Required dependencies

- `nodejs` — version 6.x.x is recommended
- `urw-fonts`
- `fontconfig`
- `freetype`
- `libstdc++`

⚠️ **These packages are CentOS 7 packages, they may have a different name in other operating systems** ⚠️

### Required environment

The web server will use operating system environment variables in order to run. Those variables and their purpose are the following :

VARIABLE                  | DESCRIPTION
------------------------- | -----------
`ENV`                     | Current environment — `development`, `staging` or `production`
`API_URL`                 | URL where Tripperty API is hosted
`LOG_PATH`                | Logs path — **relative to server index.js**
`DB_SRV`                  | Database server URL — with port, if needed
`DB_USR`                  | Database user
`DB_PWD`                  | Database password
`DB_DB`                   | Database name
`STRIPE_API_KEY`          | Stripe API key
`AWS_ACCESS_KEY`          | Amazon Web Services access key
`AWS_SECRET_KEY`          | Amazon Web Services secret key
`GOOGLE_CLIENT_ID`        | Google Client ID for back-end — « Other platforms »
`GOOGLE_CLIENT_ID_IOS`    | Google Client ID for iOS
`GOOGLE_CLIENT_ID_WEB`    | Google Client ID for front-end — « Web client »
`TWILIO_SID`              | Twilio secret ID
`TWILIO_TOKEN`            | Twilio access token
`ORDER_EXPORT_RECIPIENTS` | Email recipients who will receive daily order exports, separated by a comma
`EMAIL_DEFAULT_ID`        | Identity to use for all emails (as sender)
`APP_DRIVER_KEY`          | Special key to use for order state updates — **only used by Tripperty driver app**
`ONESIGNAL_API_KEY`       | OneSignal API key
`ONESIGNAL_REST_API_KEY`  | OneSignal REST API key

### Optionnal environment

VARIABLE | DESCRIPTION
---------|------------
`PORT`   | Port where Tripperty server listens — **default:** `3002`

### Variables

You can find some environment variables in `tripperty.env`. Required environment variables which are not present in this file must be created through corresponding service.

## Services

### HAPI

Tripperty web server uses HAPI framework to run.

- [Official website](https://hapijs.com)
- [GitHub](https://github.com/hapijs/hapi)

### Amazon Web Services

AWS Simple Email Service (SES) and Simple Notification Service (SNS) are used respectively to send emails and SMS.

You can find AWS account credentials in `tripperty.credentials` file.

- [AWS console](https://console.aws.amazon.com)
- [AWS SES](https://console.aws.amazon.com/ses/home)
- [AWS SNS](https://console.aws.amazon.com/sns/v2/home)

### Google APIs

Google Signin API is used to provide registration to Tripperty service through Google accounts.

- [Google Console](https://console.developers.google.com)
- [Documentation](https://developers.google.com/identity/sign-in/web/backend-auth#using-a-google-api-client-library)
- [GitHub](https://github.com/google/google-auth-library-nodejs)

### OneSignal

OneSignal is used to send push notifications to customers when an order state is updated.

- [Official website](https://onesignal.com/)
- [Documentation](https://documentation.onesignal.com/)

### Stripe

Stripe is used to process payments and refunds.

- [Official website](https://stripe.com)
- [Documentation](https://stripe.com/docs)

### Twilio Lookup

Twilio Lookup is used to convert national phone numbers to international phone numbers.

- [Official website](https://www.twilio.com)
- [Documentation](https://www.twilio.com/docs/api/lookups)
- [Lookup code example](https://github.com/TwilioDevEd/api-snippets/blob/master/lookups/lookup-get-basic-example-2/lookup-get-basic-example-2.2.x.js)

## Database

Tripperty web server uses `mongoDB` to handle data and is using the following structure.

### admin

```
{
  login: {
    type: String,
    required: true,
    index: true
  },
  password: {
    type: String,
    required: true
  },
  scope: {
    type: String,
    default: 'admin',
    enum: ['admin']
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
}
```

### invite

```
{
  sponsor: {
    type: String,
    required: true,
    ref: 'User'
  },
  invitee: {
    type: String,
    required: true,
    index: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  }
}
```

### landing

```
{
  email: {
    type: String,
    required: true,
    index: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
}
```

### order

```
{
  number: {
    type: String,
    index: true,
    required: true
  },
  pickup: {
    address: {
      type: String,
      required: true
    },
    date: {
      type: Date,
      required: true
    }
  },
  delivery: {
    address: {
      type: String,
      default: 'Baggages du Monde - Aéroport CDG - Terminal 2E'
    },
    latitude: {
      type: String,
      default: '49.009691'
    },
    longitude: {
      type: String,
      default: '2.547925'
    },
    date: {
      type: Date,
      required: true
    }
  },
  luggages: {
    type: Number,
    required: true
  },
  storage: {
    type: Number,
    default: 0
  },
  isToggledRegistrationZone{
    type:boolean
    },
  price: {
    total: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    includedLuggage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    extraLuggage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    storage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    registrationZone{
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    discount: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    }
  },
  stripeCharge: {
    type: String,
    default: ''
  },
  isCgvChecked:{
    type: Boolean,
  },
  stripeRefund: {
    type: String,
    default: ''
  },
  cardLast4: {
    type: String
  },
  cardBrand: {
    type: String
  },
  cardId: {
    type: String
  },
  states: [{
    id: {
      type: String,
      enum: [
        'IN_PROCESS',
        'ORDERED',
        'WAIT_FOR_PICKUP',
        'IN_TRANSIT',
        'ARRIVED',
        'DELIVERED',
        'ANOMALY',
        'REFUNDED'
      ]
    },
    date: {
      type: Date
    },
    eta: {
      type: Date
    }
  }],
  flightNumber: {
    type: String,
    default: ''
  },
  user: {
    type: String,
    required: true,
    ref: 'User'
  },
  rating: {
    type: Number,
    default: 0
  },
  customerTimeArrival: {
    type: Date
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
}
```

Orders also have a virtual property `state` giving order’s last state.

### product

```
{
  product: {
    type: String,
    required: true,
    index: true
  },
  price: {
    type: Number,
    required: true
  },
  maxBundle: {
    type: Number
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
}
```

### user

```
{
  providerId: {
    type: String,
    index: true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  profilePicture: {
    type: String
  },
  password: {
    type: String
  },
  email: {
    email: {
      type: String,
      required: true,
      index: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    verifyToken: {
      type: String,
      default: () => uuid()
    },
    resetToken: String,
    tokenExpiration: {
      type: Date,
      default: () => moment().add(1, 'day')
    },
    markedAsSpam: {
      type: Boolean,
      default: false
    }
  },
  phone: {
    phone: {
      type: String,
      required: true,
      index: true
    },
    international: {
      type: String,
      required: true,
      index: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    verifyToken: {
      type: String,
      default: () => randomString(6)
    },
    tokenExpiration: {
      type: Date,
      default: () => moment().add(1, 'day')
    },
    regenCount: {
      type: Number,
      default: 0
    },
    lastRegen: Date
  },
  billing: {
    firstname: String,
    lastname: String,
    company: String,
    address: String,
    vat: String
  },
  sponsor: {
    type: String,
    ref: 'User'
  },
  sponsorRewarded: {
    type: Boolean,
    default: false
  },
  accountBalance: {
    type: Number,
    default: 0
  },
  stripeId: {
    type: String,
    default: ''
  },
  playerIds: [{
    type: String
  }],
  language: {
    type: String,
    enum: ['fr', 'en'],
    required: true
  },
  scope: {
    type: String,
    default: 'user',
    enum: ['user']
  },
  active: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
}
```

## Web server

### Build

1. Run a terminal from source code folder
2. Run `npm install` to install project dependencies
3. Installer mangoDB
https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/
4. Dans `package.json`, vérifier :
```
"DB_SRV": "localhost:3003",
"DB_DB": "tripperty",
"DB_USR": "admin",
"DB_PWD": "trippertyadminyellowinnovation"
```
5. Dans un premier terminal:
```
sudo mongod --port 3003 --dbpath /data/db
```
6. Dans un second terminal
```
mongo --port 3003
```
```
use tripperty
```
```
db.createUser({
    user: "admin",
    pwd: "trippertyadminyellowinnovation",
    roles: [{role: "userAdmin", db: "tripperty"}]
})
```
```
exit
```
7. Dans le premier terminal, restart mongod (Garder la console ouverte par la suite)
```
exit
```
```
sudo mongod --port 3003 --dbpath /data/db
```
8. Vérifier que l'user a bien été créé
```
mongo --port 3003 -u "admin" -p "trippertyadminyellowinnovation" --authenticationDatabase "tripperty"
```
```
exit
```
9. Run `npm run build` to build Tripperty web server

Build output is located under `build` folder. This folder will contain two directories and one file :

- `node_modules` : project dependencies
- `server` : Tripperty web server code
- `package.json`

### Start

```sh
# Development environment
$ npm run dev

# Staging / Production environment
$ npm start
```

It is recommended to use process monitoring tools like `pm2` to start and handle process lifecycle.

## Internationalization

### How does it work?

To share translations between every Tripperty platform (web, mobile app and server), we use a web published Google Spreadsheet with unique IDs matching several translations.

### How to publish Spreadsheet?

- Open the Spreadsheet
- Go to `File` — `Publish to the web`
- In the dialog, choose `Link` tab
- Choose to publish `Whole document` as a `Web page`
- Click on `Publish`
- Get Spreadsheet ID : https://docs.google.com/spreadsheets/d/SPREADSHEET_ID/pubhtml

⚠️ **MAKE SURE TO UPDATE SPREADSHEET ID FOR ALL TRIPPERTY PLATFORMS** ⚠️
