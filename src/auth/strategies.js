import config from './config'

export default function (server) {
  server.auth.strategy('jwt', 'jwt', config)
  server.auth.default('jwt')
}
