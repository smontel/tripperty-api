const aws = {
  accessKeyId: process.env.AWS_ACCESS_KEY,
  secretAccessKey: process.env.AWS_SECRET_KEY
}
const mongo = {
  host: `${process.env.DB_SRV}/${process.env.DB_DB}`,
  user: process.env.DB_USR,
  pwd: process.env.DB_PWD
}
const twilio = {
  sid: process.env.TWILIO_SID,
  authToken: process.env.TWILIO_TOKEN
}

const apiURL = process.env.API_URL
const defaultIdentity = process.env.EMAIL_DEFAULT_ID
const oneSignalAppId = process.env.ONESIGNAL_APP_ID
const oneSignalRestApiKey = process.env.ONESIGNAL_REST_API_KEY
const snsRegion = process.env.AWS_SNS_REGION
const snsTopic = process.env.AWS_SNS_TOPIC
const stripe = process.env.STRIPE_API_KEY
const supportedProviders = ['google', 'facebook']

export {
  apiURL,
  aws,
  defaultIdentity,
  mongo,
  oneSignalAppId,
  oneSignalRestApiKey,
  snsRegion,
  snsTopic,
  stripe,
  supportedProviders,
  twilio
}
