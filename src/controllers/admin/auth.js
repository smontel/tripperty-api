import bcrypt from 'bcrypt'
import Boom from 'boom'
import Joi from 'joi'

import Admin from 'database/models/admin'
import { createToken } from 'utils'

const auth = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      login: Joi.string().required(),
      password: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Admin.findOne({ login: request.payload.login }, (err, admin) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!admin) {
          return reply(Boom.notFound('error.unknownLogin'))
        }

        admin.scope = 'admin'

        bcrypt.compare(request.payload.password, admin.password, (err, isValid) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!isValid) {
            return reply(Boom.badRequest('error.wrongPassword'))
          }

          return reply(admin.toObject())
        })
      })
    },
    assign: 'admin'
  }],
  handler: (request, reply) => reply({ token: createToken(request.pre.admin) }),
  description: 'Authenticate admin',
  notes: 'Return a JSON web token valid for one hour.',
  tags: ['api', 'auth', 'admin']
}

export default auth
