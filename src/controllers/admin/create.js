import Boom from 'boom'
import Joi from 'joi'

import Admin from 'database/models/admin'
import { hashPassword } from 'utils'

const create = {
  auth: {
    scope: 'admin'
  },
  validate: {
    payload: Joi.object().keys({
      login: Joi.string().required(),
      password: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Admin.findOne({ login: request.payload.login }, (err, admin) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (admin) {
          return reply(Boom.conflict('error.loginExists'))
        }

        return reply(request.payload)
      })
    }
  }],
  handler: (request, reply) => {
    const admin = new Admin({
      login: request.payload.login
    })

    hashPassword(request.payload.password, (err, hash) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      admin.password = hash

      admin.save((err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        admin.scope = 'admin'

        return reply(admin).code(201)
      })
    })
  },
  description: 'Create admin',
  tags: ['api', 'admin']
}

export default create
