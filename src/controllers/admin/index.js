import auth from './auth'
import create from './create'
import update from './update'
import read from './read'
import remove from './remove'

export default {
  auth,
  create,
  read,
  remove,
  update
}
