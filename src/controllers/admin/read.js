import Boom from 'boom'
import Joi from 'joi'

import Admin from 'database/models/admin'

const read = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      admin: Joi.string().empty('')
    })
  },
  handler: (request, reply) => {
    if (request.params.admin) {
      Admin.findById(request.params.admin, (err, admin) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply(admin)
      })
    } else {
      Admin.find((err, admins) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply(admins)
      })
    }
  },
  description: 'Get admin info',
  notes: 'If param `admin` is empty, get all admins info.',
  tags: ['api', 'admin']
}

export default read
