import Boom from 'boom'
import Joi from 'joi'

import Admin from 'database/models/admin'

const remove = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      admin: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      if (request.auth.credentials._id === request.params.admin) {
        return reply(Boom.forbidden('error.deleteOwnAccount'))
      }

      Admin.find((err, admins) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (admins.length <= 1) {
          return reply(Boom.locked('error.deleteLastRemainingAdmin'))
        }

        Admin.findById(request.params.admin, (err, admin) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!admin) {
            return reply(Boom.notFound('error.unknownAdmin'))
          }

          return reply(request.payload)
        })
      })
    }
  }],
  handler: (request, reply) => {
    Admin.findByIdAndRemove(request.params.admin, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Remove admin',
  notes: 'At least one admin must remain in DB, admin cannot remove their own account.',
  tags: ['api', 'admin']
}

export default remove
