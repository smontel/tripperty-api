import Boom from 'boom'
import Joi from 'joi'

import Admin from 'database/models/admin'
import { hashPassword } from 'utils'

const update = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      admin: Joi.string().required()
    }),
    payload: Joi.object().keys({
      password: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Admin.findById(request.params.admin, (err, admin) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!admin) {
          return reply(Boom.notFound('error.unknownAdmin'))
        }

        return reply(request.payload)
      })
    }
  }],
  handler: (request, reply) => {
    const $set = {
      updatedOn: Date.now()
    }

    hashPassword(request.payload.password, (err, hash) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      $set.password = hash

      Admin.findByIdAndUpdate(request.params.admin, { $set }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply({
          acknowledge: true
        })
      })
    })
  },
  description: 'Update admin',
  tags: ['api', 'admin']
}

export default update
