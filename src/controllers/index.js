import admin from './admin'
import invite from './invite'
import landing from './landing'
import order from './order'
import product from './product'
import root from './root'
import sesBouncesComplaints from './ses-bounces-and-complaints'
import user from './user'
import verify from './verify'
import testing from './testing'

export default {
  admin,
  invite,
  landing,
  order,
  product,
  root,
  user,
  verify,
  sesBouncesComplaints,
  testing
}
