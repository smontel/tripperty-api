import invite from './invite'
import rewardSponsor from './rewardSponsor'

export default {
  invite,
  rewardSponsor
}
