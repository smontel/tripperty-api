import Boom from 'boom'

import Invite from 'database/models/invite'
import User from 'database/models/user'
import { sendInvite } from 'emails'
import { Joi } from 'utils'

const invite = {
  auth: {
    scope: 'user'
  },
  validate: {
    payload: Joi.object().keys({
      email: Joi.alternatives().try(Joi.string().email(), Joi.array().items(Joi.string().email()))
    })
  },
  pre: [{
    method: (request, reply) => {
      User.findOne({ 'email.email': request.payload.email }, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (user) {
          return reply(Boom.conflict('error.emailExists'))
        }

        User.findById(request.auth.credentials._id, (err, user) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!user) {
            return reply(Boom.notFound(`error.userNotFound`))
          }

          Invite.findOne({ invitee: request.payload.email }, (err, invitee) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            if (invitee) {
              return reply(Boom.badRequest('error.emailInvited'))
            }

            return reply(user.toObject())
          })
        })
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const emails = Array.isArray(request.payload.email) ? request.payload.email : [request.payload.email]

    const invitations = emails.map((email) => {
      sendInvite(request.server.app.aws.ses, request.pre.user, email)

      return {
        sponsor: request.pre.user._id,
        invitee: email
      }
    })

    Invite.insertMany(invitations, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Invite someone by email',
  tags: ['api', 'invite']
}

export default invite
