import Promise from 'bluebird'

import User from 'database/models/user'

const rewardSponsor = (userId, sponsorId) => new Promise((resolve, reject) => {
  User.findById(sponsorId, (err, sponsor) => {
    if (err) {
      return reject(err)
    }

    if (!sponsor) {
      return reject('error.sponsorNotFound')
    }

    User.findByIdAndUpdate(sponsorId, { $set: { accountBalance: sponsor.accountBalance + 10 } }, (err) => {
      if (err) {
        return reject(err)
      }

      User.findByIdAndUpdate(userId, { $set: { sponsorRewarded: true } }, (err) => {
        if (err) {
          return reject(err)
        }

        return resolve()
      })
    })
  })
})

export default rewardSponsor
