import Boom from 'boom'

import Landing from 'database/models/landing'
import { Joi } from 'utils'

const create = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      email: Joi.string().email().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Landing.findOne({ email: request.payload.email }, (err, email) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (email) {
          return reply(Boom.conflict())
        }

        return reply()
      })
    }
  }],
  handler: (request, reply) => {
    const add = new Landing({
      email: request.payload.email
    })

    add.save((err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Save emails from landing page',
  tags: ['api', 'landing']
}

export default create
