import Boom from 'boom'

import Landing from 'database/models/landing'

const read = {
  auth: {
    scope: 'admin'
  },
  handler: (request, reply) => {
    Landing.find((err, emails) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(emails)
    })
  },
  description: 'Get landing emails',
  tags: ['api', 'landing']
}

export default read
