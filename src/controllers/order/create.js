import Boom from 'boom'
import moment from 'moment'

import { orderNotCompletedCron } from 'crons'
import Order from 'database/models/order'
import { Joi } from 'utils'

const create = {
  auth: {
    strategy: 'jwt',
    mode: 'try'
  },
  validate: {
    payload: Joi.object().keys({
      pickup: Joi.object().keys({
        address: Joi.string().empty('').required(),
        date: Joi.date().atLeastTomorrow().required()
      }).required(),
      delivery: Joi.object().keys({
        date: Joi.date().atLeastTomorrow().required()
      }),
      luggages: Joi.number().min(1).required(),
      flightNumber: Joi.string(),
      isCgvChecked: Joi.boolean().required(),
      isToggledRegistrationZone: Joi.boolean(),
      cardBrand: Joi.string().empty(''),
      cardLast4: Joi.string().empty(''),
      cardId: Joi.string().empty(''),
      save: Joi.boolean().default(true)
    })
  },
  pre: [{
    method: (request, reply) => {
      if (moment().hour() >= 16 && moment(request.payload.pickup.date).diff(moment(), 'days') <= 1) {
        return reply(Boom.badRequest('error.tooLateToCreateOrderForTomorrow'))
      } else if (request.payload.isCgvChecked === false) {
        return reply(Boom.badRequest('error.CgvNotChecked'))
      }
      return reply()
    }
  }],
  handler: (request, reply) => {
    const date = new Date()
    const d = {
      y: date.getFullYear().toString().substr(2, 2),
      m: date.getMonth() + 1,
      d: date.getDate()
    }

    let orderNumber = `${d.y}${d.m < 10 ? `0${d.m}` : d.m}${d.d}`

    Order.findOne({ number: new RegExp(`^${orderNumber}[0-9]{3}$`) })
      .sort('-number')
      .exec((err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (order) {
          orderNumber = +order.number + 1
        } else {
          orderNumber = `${orderNumber}001`
        }

        const storage = moment(request.payload.delivery.date).diff(request.payload.pickup.date, 'days')
        request.server.inject({
          method: 'POST',
          url: '/product/calc',
          credentials: request.auth.credentials,
          payload: {
            luggages: request.payload.luggages,
            isToggledRegistrationZone: request.payload.isToggledRegistrationZone,
            // isCgvChecked: request.payload.isCgvChecked,
            storage
          }
        }, (res) => {
          if (res.statusCode === 200) {
            const price = {
              total: res.result.total,
              includedLuggage: res.result.default
            }

            if (res.result.extraLuggage) {
              price.extraLuggage = res.result.extraLuggage
            }

            if (res.result.storage) {
              price.storage = res.result.storage
            }

            if (res.result.discount) {
              price.discount = res.result.discount
            }
            if (res.result.registrationZone) {
              price.registrationZone = res.result.registrationZone
            }
            order = new Order({
              number: orderNumber,
              pickup: request.payload.pickup,
              luggages: request.payload.luggages,
              isCgvChecked: request.payload.isCgvChecked,
              isToggledRegistrationZone: request.payload.isToggledRegistrationZone,
              delivery: {
                date: request.payload.delivery.date
              },
              storage,
              states: [{
                id: 'IN_PROCESS',
                date: Date.now()
              }],
              flightNumber: request.payload.flightNumber,
              user: request.auth.credentials ? request.auth.credentials._id : null,
              price,
              cardLast4: request.payload.cardLast4,
              cardBrand: request.payload.cardBrand,
              cardId: request.payload.cardId
            })

            if (!request.payload.save) {
              return reply(order)
            }

            order.save((err) => {
              if (err) {
                return reply(Boom.wrap(err))
              }

              const tomorrow = moment().add(1, 'days')
              const totomorrow = moment().add(2, 'days')
              const pickupDate = new Date(order.pickup.date)

              let daysToSend = 0

              if (moment(pickupDate).isSame(totomorrow, 'day')) {
                daysToSend = 1
              } else if (!moment(pickupDate).isSame(tomorrow, 'day') && !moment(pickupDate).isSame(moment(), 'day')) {
                daysToSend = 2
              }

              if (daysToSend) {
                orderNotCompletedCron(request.server.app.aws.ses, order.number, daysToSend)
              }

              return reply(order).code(201)
            })
          } else {
            return reply(Boom.wrap(res.result))
          }
        })
      })
  },
  description: 'Create order',
  notes: 'State `IN_PROCESS` is automagically added.',
  tags: ['api', 'order']
}

export default create
