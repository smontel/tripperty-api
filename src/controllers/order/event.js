import Boom from 'boom'
import iCalendar from 'icalendar'
import Joi from 'joi'

import Order from 'database/models/order'
import { generateEventData, uuid } from 'utils'

const event = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order, user: request.auth.credentials._id }, (err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        return reply(order)
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    const eventData = generateEventData(request.pre.order)

    if (request.query && request.query.format && request.query.format === 'ical') {
      const event = new iCalendar.VEvent(uuid())

      event.setSummary(eventData.summary)
      event.setDescription(eventData.description)
      event.setLocation(eventData.location)
      event.setDate(eventData.start, eventData.end)

      return reply(event.toString()).type('text/calendar')
    }

    return reply(eventData)
  },
  description: 'Generate order pickup event data',
  notes: 'Allow a GET parameter `?format=ical` to get a generated .ical file.',
  tags: ['api', 'order', 'event', 'pickup']
}

export default event
