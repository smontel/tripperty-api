import Boom from 'boom'
import fs from 'fs'
import Handlebars from 'handlebars'
import pdf from 'html-pdf'
import Joi from 'joi'
import path from 'path'

import { sendOrderInvoice } from 'emails'
import Order from 'database/models/order'
import Product from 'database/models/price'
import { getFormattedDateTime, getPricing } from 'utils'

const invoice = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required(),
      format: Joi.string().valid(['email'])
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order, user: request.auth.credentials._id })
        .populate('user')
        .exec((err, order) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!order) {
            return reply(Boom.notFound('error.orderNotFound'))
          }

          return reply(order.toObject())
        })
    },
    assign: 'order'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'default' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'defaultProduct'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'extra' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'extraProduct'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'storage' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'storageProduct'
  }],
  handler: (request, reply) => {
    const order = request.pre.order
    const context = {
      pricing: {
        default: {
          ttc: request.pre.defaultProduct.price,
          tva: 20
        },
        extraLuggage: {
          ttc: request.pre.extraProduct.price,
          tva: 20
        },
        storage: {
          ttc: request.pre.storageProduct.price,
          tva: 20
        },
        registrationZone: {
          ttc: request.pre.registrationZone.price,
          tva: 20
        },
        registrationZoneExtra: {
          ttc: request.pre.registrationZoneExtra.price,
          tva: 20
        }
      }
    }

    Object.keys(context.pricing).forEach((product) => {
      const ht = context.pricing[product].ttc / ((100 + context.pricing[product].tva) / 100)

      context.pricing[product].ht = (Math.round(ht * 100) / 100).toString()
    })

    order.date = getFormattedDateTime(order.createdOn, 'dd/mm/yyyy')

    if (order.luggages > request.pre.defaultProduct.maxBundle) {
      order.extraLuggage = order.luggages - request.pre.defaultProduct.maxBundle
    }

    order.pricing = getPricing(
      context,
      order.luggages > request.pre.defaultProduct.maxBundle ? request.pre.defaultProduct.maxBundle : order.luggages,
      order.extraLuggage,
      order.storage
    )

    context.order = order

    Object.keys(context.pricing).forEach((product) => {
      context.pricing[product].ttc = (Math.round(context.pricing[product].ttc * 100) / 100).toString()
      context.pricing[product].tva = (Math.round(context.pricing[product].tva * 100) / 100).toString()
    })

    fs.readFile(path.join(__dirname, '..', '..', 'html', 'invoice.html'), 'utf8', (err, source) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      const template = Handlebars.compile(source)
      const html = template(context)

      const options = {
        format: 'A4',
        orientation: 'portrait',
        border: '1cm',
        footer: {
          height: '1cm',
          contents: {
            default: '<div style="color: #777;font-size: .2cm;text-align: center;padding-top: .25cm">La Poste - Société Anonyme au capital de 3 800 000 000 euros - 356 000 000 RCS PARIS Siège social : 44 Boulevard de Vaugirard - 75757 PARIS CEDEX 15 - Siret 356 000 000 00048 N° de TVA intra-communautaire : FR 39 356 000 000</div>'
          }
        }
      }

      if (request.params.format === 'email') {
        if (order.user.email.verified) {
          pdf.create(html, options).toBuffer((err, buffer) => {
            if (err) {
              reply(Boom.wrap(err))
            }

            const pdfContent = buffer.toString('base64')

            sendOrderInvoice(pdfContent, order).then(
              (response) => reply({ acknowledge: true }),
              (reason) => reply(Boom.wrap(reason))
            )
          })
        } else {
          return reply(Boom.badRequest('error.emailNotVerified'))
        }
      } else {
        pdf.create(html, options).toFile(`./fac-${order.number}.pdf`, (err, res) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          reply.file(res.filename).type('application/pdf')

          setTimeout(() => fs.unlink(res.filename), 60000)
        })
      }
    })
  }
}

export default invoice
