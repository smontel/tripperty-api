import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'
import User from 'database/models/user'
import { sendOrderConfirmation } from 'emails'

const sendOrderConfirmationEmail = (ses, order, user, charge, reply) => {
  sendOrderConfirmation(ses, order, user)
    .then(() => reply({
      acknowledge: true,
      charge
    }))
    .catch((err) => reply(Boom.wrap(err)))
}

const pay = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    }),
    payload: Joi.object().keys({
      card: Joi.string()
    }).allow(null)
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order, user: request.auth.credentials._id }).populate('user').exec((err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        const lastState = order.states[order.states.length - 1].id

        if (lastState !== 'IN_PROCESS') {
          return reply(Boom.forbidden('error.orderProcessedOrCanceled'))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    const order = request.pre.order

    const source = request.payload ? {
      source: request.payload.card
    } : {}

    request.server.app.stripe.charges.create({
      amount: order.price.total.ttc * 100,
      currency: 'eur',
      customer: order.user.stripeId,
      ...source
    }).then((charge) => {
      order.states.push({
        date: Date.now(),
        id: 'ORDERED'
      })

      const $set = {
        states: order.states,
        stripeCharge: charge.id,
        cardLast4: charge.source.last4,
        cardBrand: charge.source.brand,
        stripeId: order.user.stripeId
      }

      Order.findByIdAndUpdate(order._id, { $set }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (order.price.discount) {
          const newBalance = order.user.accountBalance - order.price.discount.ttc

          User.findByIdAndUpdate(order.user._id, { $set: { accountBalance: newBalance } }, (err) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            sendOrderConfirmationEmail(request.server.app.aws.ses, order, order.user, charge, reply)
          })
        } else {
          sendOrderConfirmationEmail(request.server.app.aws.ses, order, order.user, charge, reply)
        }
      })
    }).catch((err) => reply(Boom.wrap(err)))
  },
  description: 'Pay an order',
  tags: ['api', 'order', 'payment']
}

export default pay
