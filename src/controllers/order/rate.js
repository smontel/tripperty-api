import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'

const rate = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    }),
    payload: Joi.object().keys({
      score: Joi.number().min(1).max(5).required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({
        _id: request.params.order,
        user: request.auth.credentials._id
      }, (err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        return reply(order)
      })
    }
  }],
  handler: (request, reply) => {
    Order.findByIdAndUpdate(request.params.order, { rating: request.payload.score }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Rate an order',
  tags: ['api', 'order', 'rate']
}

export default rate
