import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'

const read = {
  auth: {
    scope: ['admin', 'user']
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().empty('')
    })
  },
  pre: [{
    method: (request, reply) => {
      if (request.auth.credentials.scope === 'user' && request.params.order) {
        Order.findOne({ _id: request.params.order, user: request.auth.credentials._id }, (err, order) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!order) {
            return reply(Boom.notFound('error.orderNotFound'))
          }

          return reply(request.params)
        })
      } else {
        return reply(request.params)
      }
    }
  }],
  handler: (request, reply) => {
    let query

    if (request.params.order) {
      query = Order.findById(request.params.order)
    } else {
      query = Order.find()
    }

    if (request.auth.credentials.scope === 'user') {
      query.where('user').equals(request.auth.credentials._id)
    }

    query.select('-__v').sort({ 'user': 'asc', 'createdOn': 'desc' }).exec((err, result) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(result)
    })
  },
  description: 'Get order info',
  notes: 'Users can only access their own orders. If param `order` is empty, list all orders.',
  tags: ['api', 'order']
}

export default read
