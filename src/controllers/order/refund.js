import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'
import User from 'database/models/user'

const refund = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order, user: request.auth.credentials._id }).populate('user').exec((err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        const lastState = order.states[order.states.length - 1].id

        if (lastState !== 'ORDERED') {
          return reply(Boom.forbidden('error.orderNotPaidOrCanceled'))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    const order = request.pre.order

    request.server.app.stripe.refunds.create({
      charge: order.stripeCharge
    }).then((refund) => {
      order.states.push({
        date: Date.now(),
        id: 'REFUNDED'
      })

      Order.findByIdAndUpdate(order._id, { $set: { states: order.states, stripeRefund: refund.id } }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (order.price.discount) {
          const newBalance = order.user.accountBalance + order.price.discount.ttc

          User.findByIdAndUpdate(order.user._id, { $set: { accountBalance: newBalance } }, (err) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            return reply({
              acknowledge: true,
              refund
            })
          })
        } else {
          return reply({
            acknowledge: true,
            refund
          })
        }
      })
    }).catch((err) => reply(Boom.wrap(err)))
  },
  description: 'Refund an order payment',
  tags: ['api', 'order', 'refund']
}

export default refund
