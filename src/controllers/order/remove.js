import Boom from 'boom'

import Order from 'database/models/order'
import { Joi } from 'utils'

const create = {
  auth: {
    scope: 'admin'
  },
  validate: {
    payload: Joi.object().keys({
      user: Joi.string().required()
    })
  },
  handler: (request, reply) => {
    Order.deleteMany({ user: request.payload.user }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({ acknowledge: true })
    })
  },
  description: 'Delete user orders',
  tags: ['api', 'order']
}

export default create
