import Boom from 'boom'
import Joi from 'joi'
import moment from 'moment'

import Order from 'database/models/order'
import { getFormattedDateTime } from 'utils'

const arrived = {
  auth: false,
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    }),
    payload: Joi.object().keys({
      accessKey: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      if (request.payload.accessKey !== process.env.APP_DRIVER_KEY) {
        return reply(Boom.unauthorized('Invalid access key.'))
      }

      Order.findOne({ _id: request.params.order }).exec((err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('Commande inconnue'))
        }

        if (!moment().isSame(order.pickup.date, 'day')) {
          const pickupDate = getFormattedDateTime(order.pickup.date)
          const diffWithToday = moment().diff(order.pickup.date, 'days')

          return reply(Boom.badRequest(`La date de retrait de la commande (${pickupDate}) ne correspond pas à la date du jour (${diffWithToday}).`))
        }

        const lastState = order.states[order.states.length - 1].id

        if (lastState !== 'IN_TRANSIT') {
          return reply(Boom.forbidden(`Anomalie dans les états de la commande, le dernier état devrait être IN_TRANSIT. Dernier état connu de la commande : ${lastState}`))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    const order = request.pre.order

    order.states.push({
      date: Date.now(),
      id: 'ARRIVED'
    })

    Order.findByIdAndUpdate(order._id, { $set: { states: order.states } }, (err) => {
      if (err) {
        return reply(Boom.badRequest(`Erreur lors de la mise à jour de la commande: ${err}`))
      }

      const promise = []

      /* if (order.user.playerIds.length > 0) {
        const data = {
          id: order._id,
          type: 'STATE_UPDATE',
          state: 'ARRIVED'
        }
        const { createNotification, sendNotification } = request.server.app.oneSignal
        const notification = createNotification(order.user.playerIds, 'notification.arrivedTitle', 'notification.arrivedContent', data)

        promise.push(sendNotification(notification))
      } */

      Promise.all(promise)
        .then(() => reply('Commande mise à jour.'))
        .catch((err) => {
          request.server.app.logger.log('error', `Fail to push notification to user ${order.user._id}. ERROR: ${err}`)

          return reply('Commande mise à jour.')
        })
    })
  },
  description: 'Set an order as arrived',
  tags: ['api', 'order', 'arrived']
}

export default arrived
