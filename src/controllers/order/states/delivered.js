import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'

const delivered = {
  auth: false,
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order }).exec((err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('Commande inconnue'))
        }

        const lastState = order.states[order.states.length - 1].id

        if (lastState !== 'ARRIVED') {
          return reply(Boom.forbidden(`Anomalie dans les états de la commande, le dernier état devrait être ARRIVED. Dernier état connu de la commande : ${lastState}`))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    const order = request.pre.order

    order.states.push({
      date: Date.now(),
      id: 'DELIVERED'
    })

    Order.findByIdAndUpdate(order._id, { $set: { states: order.states } }, (err) => {
      if (err) {
        return reply(Boom.badRequest(`Erreur lors de la mise à jour de la commande: ${err}`))
      }

      return reply('Commande mise à jour.')
    })
  },
  description: 'Set an order as delivered',
  tags: ['api', 'order', 'delivered']
}

export default delivered
