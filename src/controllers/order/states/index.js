import arrived from './arrived'
import delivered from './delivered'
import inTransit from './inTransit'
import waitForPickup from './waitForPickup'

export default {
  arrived,
  delivered,
  inTransit,
  waitForPickup
}
