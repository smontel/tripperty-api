import Boom from 'boom'
import moment from 'moment'

import Order from 'database/models/order'
import { deepMergeUpdates, Joi } from 'utils'

const update = {
  auth: {
    scope: 'user'
  },
  validate: {
    params: Joi.object().keys({
      order: Joi.string().required()
    }),
    payload: Joi.object().keys({
      pickup: Joi.object().keys({
        address: Joi.string().empty(''),
        date: Joi.date().atLeastTomorrow()
      }),
      delivery: Joi.object().keys({
        date: Joi.date().atLeastTomorrow()
      }),
      luggages: Joi.number().min(1),
      flightNumber: Joi.string(),
      isToggledRegistrationZone: Joi.boolean(),
      isCgvChecked: Joi.boolean().required().valid(true),
      cardBrand: Joi.string(),
      cardLast4: Joi.string(),
      cardId: Joi.string(),
      customerTimeArrival: Joi.date()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.params.order, user: request.auth.credentials._id }, (err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        const isTomorrow = moment().hour() >= 16 && moment(order.pickup.date).calendar().indexOf('Tomorrow') !== -1
        const isInLessThan8HoursOrPassed = moment().diff(moment(order.pickup.date), 'hours') > -8

        if (order.state !== 'IN_PROCESS' || (order.state === 'ORDERED' && (isTomorrow || isInLessThan8HoursOrPassed))) {
          return reply(Boom.badRequest('error.tooLateToCancelOrUpdateOrder'))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  }],
  handler: (request, reply) => {
    let $set = {}

    if (request.payload.delivery && request.payload.delivery.date !== request.pre.order.delivery.date ||
      request.payload.pickup && request.payload.pickup.date !== request.pre.order.pickup.date) {
      const deliveryDate = request.payload.delivery && request.payload.delivery.date || request.pre.order.delivery.date
      const pickupDate = request.payload.pickup && request.payload.pickup.date || request.pre.order.pickup.date

      $set.storage = moment(deliveryDate).diff(pickupDate, 'days')
    }

    $set = deepMergeUpdates(request.pre.order, request.payload)
    request.server.inject({
      method: 'POST',
      url: '/product/calc',
      credentials: request.auth.credentials,
      payload: {
        luggages: request.payload.luggages || request.pre.order.luggages,
        storage: $set.storage ? $set.storage : request.pre.order.storage,
        isToggledRegistrationZone: request.payload.isToggledRegistrationZone
      }
    }, (res) => {
      if (res.statusCode === 200) {
        $set.price = {
          total: res.result.total,
          includedLuggage: res.result.default
        }

        if (res.result.extraLuggage) {
          $set.price.extraLuggage = res.result.extraLuggage
        }

        if (res.result.storage) {
          $set.price.storage = res.result.storage
        }

        Order.findByIdAndUpdate(request.params.order, { $set }, (err) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          return reply({
            acknowledge: true
          })
        })
      } else {
        return reply(Boom.wrap(res.result))
      }
    })
  },
  description: 'Update order',
  notes: 'Route payload is merged with original order, updating only changed fields.',
  tags: ['api', 'order']
}

export default update
