import Boom from 'boom'
import Joi from 'joi'

import Product from 'database/models/price'
import User from 'database/models/user'
import { getPricing } from 'utils'

const calc = {
  auth: {
    strategy: 'jwt',
    mode: 'try'
  },
  validate: {
    payload: Joi.object().keys({
      luggages: Joi.number().min(1).required(),
      storage: Joi.number().min(0),
      isToggledRegistrationZone: Joi.boolean()
    })
  },
  pre: [{
    method: (request, reply) => {
      Product.findOne({ product: 'default' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'defaultProduct'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'extra' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'extraProduct'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'registrationZoneExtra' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'registrationZoneExtra'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'registrationZone' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'registrationZone'
  }, {
    method: (request, reply) => {
      Product.findOne({ product: 'storage' }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.missingPrices'))
        }

        return reply(product)
      })
    },
    assign: 'storageProduct'
  }, {
    method: (request, reply) => {
      if (request.auth.credentials && request.auth.credentials._id) {
        User.findById(request.auth.credentials._id, (err, user) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          if (!user) {
            return reply(Boom.notFound('error.userNotFound'))
          }

          return reply(user.accountBalance)
        })
      } else {
        return reply(0)
      }
    },
    assign: 'accountBalance'
  }],
  handler: (request, reply) => {
    const context = {
      pricing: {
        default: {
          ttc: request.pre.defaultProduct.price,
          tva: 20
        },
        extraLuggage: {
          ttc: request.pre.extraProduct.price,
          tva: 20
        },
        storage: {
          ttc: request.pre.storageProduct.price,
          tva: 20
        },
        registrationZone: {
          ttc: request.pre.registrationZone.price,
          tva: 20
        },
        registrationZoneExtra: {
          ttc: request.pre.registrationZoneExtra.price,
          tva: 20
        }
      },
      accountBalance: request.pre.accountBalance
    }

    return reply(
      getPricing(
        context,
        request.payload.luggages > request.pre.defaultProduct.maxBundle ? request.pre.defaultProduct.maxBundle : request.payload.luggages,
        request.payload.luggages > request.pre.defaultProduct.maxBundle ? request.payload.luggages - request.pre.defaultProduct.maxBundle : 0,
        request.payload.storage,
        request.payload.isToggledRegistrationZone
      )
    )
  },
  description: 'Calc cart price',
  tags: ['api', 'product']
}

export default calc
