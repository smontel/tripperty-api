import Boom from 'boom'
import Joi from 'joi'

import Product from 'database/models/price'

const create = {
  auth: {
    scope: 'admin'
  },
  validate: {
    payload: Joi.object().keys({
      maxBundle: Joi.number(),
      product: Joi.string().required(),
      price: Joi.number().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Product.findOne({ product: request.payload.product }, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (product) {
          return reply(Boom.conflict('error.productExists'))
        }

        return reply()
      })
    }
  }],
  handler: (request, reply) => {
    const product = new Product({
      product: request.payload.product,
      price: request.payload.price
    })

    if (request.payload.maxBundle) {
      product.maxBundle = request.payload.maxBundle
    }

    product.save((err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(product).code(201)
    })
  },
  description: 'Create product',
  tags: ['api', 'price']
}

export default create
