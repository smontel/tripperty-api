import calc from './calc'
import create from './create'
import read from './read'
import remove from './remove'
import update from './update'

export default {
  calc,
  create,
  read,
  remove,
  update
}
