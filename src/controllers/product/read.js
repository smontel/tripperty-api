import Boom from 'boom'
import Joi from 'joi'

import Product from 'database/models/price'

const read = {
  auth: false,
  validate: {
    params: Joi.object().keys({
      product: Joi.string().empty('')
    })
  },
  handler: (request, reply) => {
    let query

    if (request.params.product) {
      query = Product.findById(request.params.product)
    } else {
      query = Product.find()
    }

    query.select('-__v').exec((err, result) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(result)
    })
  },
  description: 'Get product info',
  tags: ['api', 'product']
}

export default read
