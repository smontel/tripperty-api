import Boom from 'boom'
import Joi from 'joi'

import Product from 'database/models/price'

const remove = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      product: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Product.findById(request.params.product, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.unknownProduct'))
        }

        return reply()
      })
    }
  }],
  handler: (request, reply) => {
    Product.findByIdAndRemove(request.params.product, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Remove product',
  tags: ['api', 'product']
}

export default remove
