import Boom from 'boom'
import Joi from 'joi'

import Product from 'database/models/price'

const update = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      product: Joi.string().required()
    }),
    payload: Joi.object().keys({
      maxBundle: Joi.number(),
      price: Joi.number().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      Product.findById(request.params.product, (err, product) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!product) {
          return reply(Boom.notFound('error.productNotFound'))
        }

        return reply()
      })
    }
  }],
  handler: (request, reply) => {
    const $set = {
      price: request.payload.price,
      updatedOn: Date.now()
    }

    if (request.payload.maxBundle) {
      $set.maxBundle = request.payload.maxBundle
    }

    Product.findByIdAndUpdate(request.params.product, { $set }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Update product price',
  tags: ['api', 'price']
}

export default update
