export default {
  auth: false,
  handler: function (request, reply) {
    if (request.auth.credentials && request.auth.credentials._id) {
      return reply({
        message: `Welcome to Tripperty API! (connected as: ${request.auth.credentials.scope})`
      })
    }

    return reply({
      message: `Welcome to Tripperty API! (please connect to use)`
    })
  },
  description: 'Welcome message',
  tags: ['api']
}
