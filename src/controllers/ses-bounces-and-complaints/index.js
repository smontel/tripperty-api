import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'
import { getFormattedDateTime } from 'utils'

const sesBouncesComplaints = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      Type: Joi.string().required(),
      Message: Joi.string().required()
    }).unknown(true)
  },
  handler: (request, reply) => {
    let msg

    try {
      msg = JSON.parse(request.payload.Message)
    } catch (e) {
      return reply(Boom.badRequest())
    }

    if (!msg.notificationType) {
      return reply(Boom.badRequest())
    }

    switch (msg.notificationType) {
      case 'Bounce':
        const { bounceType, bouncedRecipients } = msg.bounce

        if (bounceType === 'Permanent' && Array.isArray(bouncedRecipients) && bouncedRecipients[0]) {
          const emailAddress = bouncedRecipients[0].emailAddress

          User.findOneAndUpdate({ email: emailAddress }, { $set: { active: false } }, (err) => {
            if (err) {
              request.server.app.logger.log('error', `${getFormattedDateTime()} : User <${emailAddress}> couldn’t be deactivated (mail address invalid, bounced): ${err}`)

              return reply()
            }

            request.server.app.logger.log('info', `${getFormattedDateTime()} : User <${emailAddress}> deactivated because mail address invalid (bounce)`)

            return reply()
          })
        } else {
          request.server.app.logger.log('error', `${getFormattedDateTime()} : Email address not found on notification: ${msg}`)
        }

        break
      case 'Complaint':
        if (Array.isArray(msg.complaint.complainedRecipients) && msg.complaint.complainedRecipients[0]) {
          User.findOne({ email: msg.complaint.complainedRecipients[0].emailAddress }, (err, user) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            if (!user) {
              request.server.app.logger.log('error', `${getFormattedDateTime()} : User <${msg.complaint.complainedRecipients[0].emailAddress}> is not a valid user`)
            }

            user.email.markedAsSpam = true

            user.save((err) => {
              if (err) {
                return reply(Boom.wrap(err))
              }

              return reply()
            })
          })
        } else {
          request.server.app.logger.log('error', `${getFormattedDateTime()} : Email address not found on notification: ${msg}`)
        }

        break
    }

    return reply()
  }
}

export default sesBouncesComplaints
