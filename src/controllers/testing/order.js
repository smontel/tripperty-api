import Boom from 'boom'
import Joi from 'joi'
import Order from 'database/models/order'

const order = {
  auth: false,
  validate: {
    query: Joi.object().keys({
      order: Joi.string().required(),
      delivery: Joi.string(),
      pickup: Joi.string(),
      reset: Joi.boolean()
    })
  },
  pre: [{
    method: (request, reply) => {
      Order.findOne({ _id: request.query.order }, (err, order) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!order) {
          return reply(Boom.notFound('error.orderNotFound'))
        }

        return reply(order.toObject())
      })
    },
    assign: 'order'
  } ],
  handler: (request, reply) => {
    const order = request.pre.order

    const $set = {}

    if (request.query.delivery) {
      $set.delivery = { ...order.delivery, date: new Date(request.query.delivery) }
    }

    if (request.query.pickup) {
      $set.pickup = { ...order.pickup, date: new Date(request.query.pickup) }
    }

    if (request.query.reset) {
      $set.states = order.states.filter(status => status.id === 'IN_PROCESS' || status.id === 'ORDERED')
    }

    Order.findByIdAndUpdate({_id: order._id}, { $set }, { new: true }, (err, newOrder) => {
      if (err) reply(Boom.wrap(err))

      return reply(newOrder)
    })
  }
}

export default order
