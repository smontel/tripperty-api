import bcrypt from 'bcrypt'
import Boom from 'boom'
import Joi from 'joi'

import { supportedProviders } from '/config'
import User from 'database/models/user'
import { createToken, omit, providerConnectionCheck } from 'utils'

const auth = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      login: Joi.string().required(),
      password: Joi.string(),
      provider: Joi.string().valid(supportedProviders),
      accessToken: Joi.string()
    }).with('provider', 'accessToken').without('password', 'provider')
  },
  pre: [{
    method: (request, reply) => {
      User.findOne({
        $or: [{
          'email.email': request.payload.login
        }, {
          'phone.phone': request.payload.login
        }, {
          providerId: request.payload.login
        }]
      }, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        if (!user.active) {
          return reply(Boom.badRequest('error.accountDeactivated'))
        }

        if (!user.phone.verified) {
          return reply(Boom.badRequest('error.phoneNotVerified'))
        }

        if (user.password) {
          if (!request.payload.password) {
            return reply(Boom.badRequest('error.missingPassword'))
          }

          bcrypt.compare(request.payload.password, user.password, (err, isValid) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            if (!isValid) {
              return reply(Boom.badRequest('error.wrongPassword'))
            }

            return reply(user.toObject())
          })
        } else {
          providerConnectionCheck(request.payload.provider)(request.payload.login, request.payload.accessToken)
            .then(
              (connected) => {
                if (!connected) {
                  return reply(Boom.badRequest('error.providerIdMismatch'))
                }

                return reply(user.toObject())
              },
              (err) => reply(Boom.wrap(err))
            )
        }
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const user = omit(request.pre.user, ['password', 'email.verifyToken', 'phone.verifyToken', 'profilePicture'])

    if (!user.stripeId) {
      user.scope = 'tmpuser'

      return reply({ message: 'miss.payment.info', token: createToken(user) }).code(301)
    }

    return reply({ token: createToken(user), user: { profilePicture: request.pre.user.profilePicture, ...user } })
  },
  description: 'Authenticate user',
  notes: 'Return a JSON web token valid for one hour.',
  tags: ['api', 'user']
}

export default auth
