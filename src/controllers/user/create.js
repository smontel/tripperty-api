import Boom from 'boom'

import { supportedProviders } from '/config'
import Invite from 'database/models/invite'
import User from 'database/models/user'
import { sendPhoneValidation } from 'sms'
import { hashPassword, Joi, omit, providerConnectionCheck } from 'utils'

const save = (user, { aws, logger }, reply) => {
  user.save((err) => {
    if (err) {
      return reply(Boom.wrap(err))
    }

    const userObject = omit(user.toObject(), ['password', 'phone', 'email'])

    sendPhoneValidation(aws.sns, user).then(
      () => reply(userObject).code(201),
      (reason) => {
        logger.log('error', reason)

        return reply(userObject).code(201)
      }
    )
  })
}

const create = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      email: Joi.string().email().required(),
      phone: Joi.string().regex(/^[0-9]+$/).required(),
      countryCode: Joi.string().min(2).max(2).required(),
      country: Joi.object().required(),
      password: Joi.string(),
      provider: Joi.string().valid(supportedProviders),
      providerId: Joi.string(),
      accessToken: Joi.string(),
      profilePicture: Joi.string(),
      language: Joi.string().valid(['fr', 'en']).required()
    }).with('providerId', 'provider').with('providerId', 'accessToken').without('password', 'providerId')
  },
  payload: {
    parse: true,
    maxBytes: 2097152
  },
  pre: [{
    method: (request, reply) => {
      if (!request.payload.password && !request.payload.providerId) {
        return reply(Boom.badRequest('error.missingPasswordOrProviderIdentifier'))
      }

      const $or = [{
        'email.email': request.payload.email
      }, {
        'phone.phone': request.payload.phone
      }]

      if (request.payload.providerId) {
        $or.push({
          providerId: request.payload.providerId
        })
      }

      User.findOne({ $or }, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (user) {
          const type = user.providerId && user.providerId === request.payload.providerId ? 'account' : user.email.email === request.payload.email ? 'email' : 'phone'

          return reply(Boom.conflict(`error.${type}Exists`))
        }

        const promise = []

        if (request.payload.provider) {
          promise.push(providerConnectionCheck(request.payload.provider)(request.payload.providerId, request.payload.accessToken))
        }

        Promise.all(promise).then(
          (connected) => {
            if (!connected) {
              return reply(Boom.badRequest('error.providerIdMismatch'))
            }

            request.server.app.getPhoneInfo(request.payload.phone, { countryCode: request.payload.countryCode }).then(
              (data) => reply(data.phoneNumber),
              (err) => reply(Boom.badRequest('error.validate.phone', err))
            )
          },
          (err) => reply(Boom.wrap(err))
        )
      })
    },
    assign: 'internationalNumber'
  }],
  handler: (request, reply) => {
    const user = new User({
      firstname: request.payload.firstname,
      lastname: request.payload.lastname,
      email: {
        email: request.payload.email
      },
      country: {
        value: request.payload.country.value,
        label: request.payload.country.label,
        prefix: request.payload.country.prefix,
      },
      phone: {
        phone: request.payload.phone,
        international: request.pre.internationalNumber,
        countryCode: request.payload.countryCode
      },
      language: request.payload.language,
      profilePicture: request.payload.profilePicture
    })

    Invite.findOne({ invitee: user.email.email })
      .populate('sponsor', '_id')
      .exec((err, invite) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (invite) {
          user.sponsor = invite.sponsor._id
        }

        if (request.payload.password) {
          hashPassword(request.payload.password, (err, hash) => {
            if (err) {
              return reply(Boom.wrap(err))
            }

            user.password = hash

            save(user, request.server.app, reply)
          })
        } else {
          user.providerId = request.payload.providerId

          save(user, request.server.app, reply)
        }
      })
  },
  description: 'Create user',
  tags: ['api', 'user']
}

export default create
