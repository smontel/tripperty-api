import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'

const deactivate = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required()
    })
  },
  handler: (request, reply) => {
    User.findByIdAndUpdate(request.params.user, { $set: { active: false } }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply({
        acknowledge: true
      })
    })
  },
  description: 'Deactivate user account',
  tags: ['api', 'user']
}

export default deactivate
