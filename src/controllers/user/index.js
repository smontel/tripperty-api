import activate from './activate'
import auth from './auth'
import create from './create'
import deactivate from './deactivate'
import orders from './orders'
import password from './password'
import payment from './payment'
import read from './read'
import regen from './regen'
import remove from './remove'
import update from './update'

export default {
  activate,
  auth,
  create,
  deactivate,
  orders,
  password,
  payment,
  read,
  regen,
  remove,
  update
}
