import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'

const orders = {
  auth: {
    scope: ['admin', 'user']
  },
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      if (request.auth.credentials.scope === 'user' && request.params.user !== request.auth.credentials._id) {
        return reply(Boom.unauthorized())
      }

      return reply(request.params)
    }
  }],
  handler: (request, reply) => {
    Order.find({ user: request.params.user }).sort({ 'createdOn': 'desc' }).exec((err, orders) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(orders)
    })
  },
  description: 'Get user orders',
  notes: 'Users can only access their own orders. If param `user` is empty and authenticated user is admin, get all user orders.',
  tags: ['api', 'user', 'orders']
}

export default orders
