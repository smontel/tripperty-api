import Boom from 'boom'
import Joi from 'joi'

import { sendPasswordReset } from 'emails'
import User from 'database/models/user'
import { uuid } from 'utils'

const forgot = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      email: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      User.findOne({ 'email.email': request.payload.email }, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        if (user.providerId) {
          return reply(Boom.badRequest('error.socialAccountHaveNoPassword'))
        }

        user.email.resetToken = uuid()
        user.updatedOn = Date.now()

        user.save((err) => {
          if (err) {
            return reply(Boom.wrap(err))
          }

          return reply(user)
        })
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    sendPasswordReset(request.server.app.aws.ses, request.pre.user).then(
      () => reply({ acknowledge: true }),
      (reason) => reply(Boom.wrap(reason))
    )
  },
  description: 'Send forgot password email to user',
  tags: ['api', 'user', 'password', 'reset']
}

export default forgot
