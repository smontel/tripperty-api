import forgot from './forgot'
import reset from './reset'

export default {
  forgot,
  reset
}
