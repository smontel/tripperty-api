import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'
import { hashPassword } from 'utils'

const forgot = {
  auth: false,
  validate: {
    payload: Joi.object().keys({
      token: Joi.string().required(),
      password: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      User.findOne({ 'email.resetToken': request.payload.token }, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.badRequest('error.invalidToken'))
        }

        return reply(user)
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const $set = {
      email: request.pre.user.email,
      updatedOn: Date.now()
    }

    $set.email.resetToken = ''

    hashPassword(request.payload.password, (err, hash) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      $set.password = hash

      User.findByIdAndUpdate(request.pre.user._id, { $set }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply({
          acknowledge: true
        })
      })
    })
  },
  description: 'Reset user password',
  tags: ['api', 'user', 'password', 'reset']
}

export default forgot
