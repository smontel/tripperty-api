import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'

const add = {
  auth: {
    scope: 'user'
  },
  validate: {
    payload: {
      source: Joi.string().required(),
      setAsMain: Joi.boolean()
    }
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const { stripe } = request.server.app
    const user = request.pre.user

    stripe.customers.createSource(user.stripeId, { source: request.payload.source })
      .then((card) => {
        if (request.payload.setAsMain) {
          stripe.customers.update(user.stripeId, {
            default_source: card.id
          }).then((customer) => reply({
            acknowledge: true
          })).catch((err) => reply(Boom.wrap(err)))
        } else {
          reply({
            acknowledge: true
          })
        }
      })
      .catch((err) => reply(Boom.wrap(err)))
  },
  description: 'Add new payment mean to user',
  tags: ['api', 'user', 'payment']
}

export default add
