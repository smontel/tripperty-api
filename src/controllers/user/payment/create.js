import Boom from 'boom'
import Joi from 'joi'

import { sendEmailValidation } from 'emails'
import User from 'database/models/user'
import { createToken, omit } from 'utils'

const create = {
  auth: {
    scope: 'tmpuser'
  },
  validate: {
    payload: {
      token: Joi.string().required()
    }
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const { aws, logger, stripe } = request.server.app
    const user = request.pre.user

    stripe.customers.create({
      email: user.email.email,
      source: request.payload.token
    }, (err, customer) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      User.findByIdAndUpdate(user._id, { $set: { stripeId: customer.id } }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        const userObject = omit(user, ['password', 'phone.verifyToken', 'profilePicture'])
        const res = {
          token: createToken(userObject)
        }

        sendEmailValidation(aws.ses, user).then(
          () => reply(res),
          (reason) => {
            logger.log('error', reason)

            return reply(res)
          }
        )
      })
    })
  },
  description: 'Create user first payment mean',
  tags: ['api', 'user', 'payment']
}

export default create
