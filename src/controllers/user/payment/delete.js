import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'

const deleteCard = {
  auth: {
    scope: 'user'
  },
  validate: {
    payload: {
      cardId: Joi.string().required()
    }
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    request.server.app.stripe.customers.deleteCard(request.pre.user.stripeId, request.payload.cardId)
      .then((confirmation) => reply({
        acknowledge: true
      }))
      .catch((err) => reply(Boom.wrap(err)))
  },
  description: 'Delete a payment card',
  tags: ['api', 'user', 'payment']
}

export default deleteCard
