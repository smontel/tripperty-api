import add from './add'
import create from './create'
import deleteCard from './delete'
import list from './list'
import setMain from './setMain'
import token from './token'

export default {
  add,
  create,
  deleteCard,
  list,
  setMain,
  token
}
