import Boom from 'boom'
import User from 'database/models/user'

const token = {
  auth: {
    scope: 'user'
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const { stripeId } = request.pre.user

    if (!stripeId) {
      return reply([])
    }

    request.server.app.stripe.customers.retrieve(stripeId)
      .then((stripeUser) => {
        const cards = stripeUser.sources.data.map((card) => ({
          ...card,
          default: (card.id === stripeUser.default_source)
        }))

        return reply(cards)
      })
  },
  description: 'List payment cards from user',
  tags: ['api', 'user', 'payment']
}

export default token
