import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'

const setMain = {
  auth: {
    scope: 'user'
  },
  validate: {
    payload: {
      source: Joi.string().required()
    }
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    request.server.app.stripe.customers.update(request.pre.user.stripeId, {
      default_source: request.payload.source
    })
      .then((customer) => reply({ acknowledge: true }))
      .catch((err) => reply(Boom.wrap(err)))
  },
  description: 'Set main payment card',
  tags: ['api', 'user', 'payment']
}

export default setMain
