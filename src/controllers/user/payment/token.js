import Boom from 'boom'

import User from 'database/models/user'

const token = {
  auth: {
    scope: 'tmpuser'
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.auth.credentials._id, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.badRequest('Unknown user'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const { stripe } = request.server.app
    stripe.tokens.create({
      card: {
        'number': '4242424242424242',
        'exp_month': 2,
        'exp_year': 2021,
        'cvc': '123'
      }
    }).then((token) => {
      reply({token})
    }).catch((err) => {
      console.log('err->', err)
      reply(Boom.wrap(err))
    })
  }
}

export default token
