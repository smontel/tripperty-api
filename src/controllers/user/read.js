import Boom from 'boom'
import Joi from 'joi'

import User from 'database/models/user'

const read = {
  auth: {
    scope: ['admin', 'user', 'tmpuser']
  },
  validate: {
    params: Joi.object().keys({
      user: Joi.string().empty('')
    })
  },
  pre: [{
    method: (request, reply) => {
      if (request.auth.credentials.scope === 'tmpuser') {
        return reply(Boom.forbidden('error.noPaymentMeanRegistered'))
      }

      if (request.auth.credentials.scope === 'user' && request.params.user && request.params.user !== request.auth.credentials._id) {
        return reply(Boom.unauthorized())
      }

      return reply()
    }
  }],
  handler: (request, reply) => {
    let query

    if (request.params.user) {
      query = User.findById(request.params.user)
    } else if (!request.params.user && request.auth.credentials.scope === 'user') {
      query = User.findById(request.auth.credentials._id)
    } else {
      query = User.find()
    }

    query.select('-password -__v -phone.verifyToken -email.verifyToken').exec((err, result) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(result)
    })
  },
  description: 'Get user info',
  notes: 'Users can only access their own info. If param `user` is empty and authenticated user is admin, get all users info.',
  tags: ['api', 'user']
}

export default read
