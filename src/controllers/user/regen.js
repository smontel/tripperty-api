import Boom from 'boom'
import Joi from 'joi'
import moment from 'moment'

import { sendEmailValidation } from 'emails'
import User from 'database/models/user'
import { sendPhoneValidation } from 'sms'
import { randomString, uuid } from 'utils'

const email = {
  auth: false,
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required(),
      phone: Joi.string()
    })
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.params.user, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        if (user.email.verified) {
          return reply(Boom.badRequest('error.emailVerified'))
        }

        reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const user = request.pre.user

    user.email.verifyToken = uuid()
    user.email.tokenExpiration = moment().add(1, 'hour')

    const $set = {
      email: user.email
    }

    User.findByIdAndUpdate(request.params.user, { $set }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      sendEmailValidation(request.server.app.aws.ses, user).then(
        () => reply({ acknowledge: true }),
        (reason) => reply(Boom.wrap(reason))
      )
    })
  },
  description: 'Regenerate a new email validation token',
  tags: ['api', 'user', 'email', 'validation']
}

const phone = {
  auth: false,
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required()
    }),
    payload: Joi.object().keys({
      phone: Joi.string(),
      countryCode: Joi.string().min(2).max(2)
    }).allow(null)
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.params.user, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        if (user.phone.verified) {
          return reply(Boom.badRequest('error.phoneVerified'))
        }

        const result = user.toObject()

        if (user.phone.regenCount >= 3 && moment().diff(user.phone.lastRegen, 'hour') === 0) {
          return reply(Boom.unauthorized('error.tooManyToken'))
        } else if (user.phone.regenCount >= 3) {
          result.phone.regenCount = 0
        }

        reply(result)
      })
    },
    assign: 'user'
  }, {
    method: (request, reply) => {
      if (request.payload && request.payload.phone && request.payload.countryCode) {
        request.server.app.getPhoneInfo(request.payload.phone, {
          countryCode: request.payload.countryCode
        })
        .then(
          (data) => reply(data.phoneNumber),
          (err) => reply(Boom.badRequest('error.validate.phone', err))
        )
      } else {
        reply(null)
      }
    },
    assign: 'internationalNumber'
  }],
  handler: (request, reply) => {
    const { _id } = request.pre.user

    const phone = request.payload && request.payload.phone && request.payload.countryCode
    ? {
      ...request.pre.user.phone,
      phone: request.payload.phone,
      international: request.pre.internationalNumber,
      countryCode: request.payload.countryCode
    } : request.pre.user.phone

    const $set = {
      phone: {
        ...phone,
        verifyToken: randomString(6),
        tokenExpiration: moment().add(1, 'hour'),
        lastRegen: moment(),
        regenCount: phone.regenCount + 1
      }
    }

    User.findByIdAndUpdate(_id, { $set }, { new: true }, (err, user) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      sendPhoneValidation(request.server.app.aws.sns, user).then(
        () => reply({ acknowledge: true }),
        (reason) => reply(Boom.wrap(reason))
      )
    })
  },
  description: 'Regenerate a new phone validation token',
  tags: ['api', 'user', 'phone', 'validation']
}

export default {
  email,
  phone
}
