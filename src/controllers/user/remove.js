import Boom from 'boom'
import Joi from 'joi'

import Order from 'database/models/order'
import User from 'database/models/user'

const remove = {
  auth: {
    scope: 'admin'
  },
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required()
    })
  },
  pre: [{
    method: (request, reply) => {
      User.findById(request.params.user, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply()
      })
    }
  }],
  handler: (request, reply) => {
    Order.find({ user: request.params.user }, (err, orders) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      if (orders.length > 0) {
        orders.forEach((order) => Order.findByIdAndRemove(order._id))
      }

      User.findByIdAndRemove(request.params.user, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply({
          acknowledge: true
        })
      })
    })
  },
  description: 'Remove user and its orders',
  tags: ['api', 'user']
}

export default remove
