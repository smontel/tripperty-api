import Promise from 'bluebird'
import Boom from 'boom'

import { sendEmailValidation } from 'emails'
import User from 'database/models/user'
import { sendPhoneValidation } from 'sms'
import { deepMergeUpdates, hashPassword, Joi, omit, randomString, uuid } from 'utils'

const checkPhoneValidation = (sns, user) => new Promise((resolve, reject) => {
  if (!user.phone.verified) {
    sendPhoneValidation(sns, user).then(
      () => resolve(),
      (reason) => reject(reason)
    )
  } else {
    resolve()
  }
})

const checkEmailValidation = (ses, user) => new Promise((resolve, reject) => {
  if (!user.email.verified) {
    sendEmailValidation(ses, user).then(
      () => resolve(),
      (reason) => reject(reason)
    )
  } else {
    resolve()
  }
})

const save = (user, update, { aws }, reply) => {
  update.updatedOn = Date.now()

  const updatedUser = {
    ...user,
    ...update
  }

  const validations = []

  if (update.email) {
    validations.push(checkEmailValidation(aws.ses, updatedUser))
  }

  if (update.phone) {
    validations.push(checkPhoneValidation(aws.sns, updatedUser))
  }

  Promise.all(validations).then(
    () => User.findByIdAndUpdate(user._id, { $set: update }, (err) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      return reply(updatedUser)
    }),
    (reason) => reply(Boom.wrap(reason))
  )
}

const update = {
  auth: {
    scope: ['admin', 'user']
  },
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required()
    }),
    payload: Joi.object().keys({
      firstname: Joi.string().required(),
      lastname: Joi.string().required(),
      password: Joi.string().empty(''),
      phone: Joi.string().required(),
      countryCode: Joi.string().min(2).max(2).required(),
      email: Joi.string().email().required(),
      profilePicture: Joi.string().empty(''),
      billing: Joi.object().keys({
        firstname: Joi.string(),
        lastname: Joi.string(),
        company: Joi.string(),
        address: Joi.string(),
        vat: Joi.string()
      }),
      oneSignalPlayerId: Joi.string(),
      notification: Joi.boolean()
    }).with('phone', 'countryCode')
  },
  payload: {
    parse: true,
    maxBytes: 2097152
  },
  pre: [{
    method: (request, reply) => {
      if (request.auth.credentials.scope === 'user' && request.params.user !== request.auth.credentials._id) {
        return reply(Boom.unauthorized())
      }

      User.findById(request.params.user, (err, user) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        if (!user) {
          return reply(Boom.notFound('error.userNotFound'))
        }

        return reply(user.toObject())
      })
    },
    assign: 'user'
  }],
  handler: (request, reply) => {
    const user = request.pre.user
    const $set = deepMergeUpdates(user, omit(request.payload, ['phone', 'email', 'password', 'oneSignalPlayerId']))

    if (request.payload.oneSignalPlayerId) {
      if (user.playerIds.indexOf(request.payload.oneSignalPlayerId) === -1) {
        user.playerIds.push(request.payload.oneSignalPlayerId)

        $set.playerIds = user.playerIds
      }
    }

    if (request.payload.email !== user.email.email) {
      $set.email = {
        email: request.payload.email,
        verifyToken: uuid(),
        verified: false
      }
    }

    if (request.payload.password) {
      hashPassword(request.payload.password, (err, hash) => {
        if (err) {
          return reply(Boom.wrap(err))
        }
        $set.password = hash
        save(request.pre.user, $set, request.server.app, reply)
      })
    }

    if (request.payload.phone !== user.phone.phone) {
      request.server.app.getPhoneInfo(request.payload.phone, { countryCode: request.payload.countryCode }).then(
        (data) => {
          $set.phone = {
            phone: request.payload.phone,
            international: data.phoneNumber,
            verifyToken: randomString(6),
            verified: false
          }
          // TODO: A DELETE
          console.log('TOKEN PHONE: ' + $set.phone.verifyToken)
          save(request.pre.user, $set, request.server.app, reply)
        },
        (err) => reply(Boom.badRequest('error.validate.phone', err))
      )
    } else {
      save(request.pre.user, $set, request.server.app, reply)
    }
  },
  description: 'Update user',
  notes: 'If phone or email is updated, these contact methods will have to be verified again.',
  tags: ['api', 'user']
}

export default update
