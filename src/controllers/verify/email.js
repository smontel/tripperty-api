import Boom from 'boom'
import Joi from 'joi'
import moment from 'moment'

import User from 'database/models/user'

const verifyEmail = ({
  auth: false,
  validate: {
    params: Joi.object().keys({
      token: Joi.string().required()
    })
  },
  handler: (request, reply) => {
    User.findOne({
      'email.verifyToken': request.params.token
    }, `_id email`, (err, user) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      if (!user) {
        return reply(Boom.badRequest('error.invalidToken'))
      }

      if (moment().isAfter(user.email.tokenExpiration)) {
        return reply(Boom.badRequest('error.expiredToken'))
      }

      const $set = {
        email: {
          email: user.email.email,
          verified: true,
          verifyToken: ''
        },
        updatedOn: Date.now()
      }

      User.findByIdAndUpdate(user._id, { $set }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        return reply({
          acknowledge: true
        })
      })
    })
  },
  description: `Verify user email`,
  tags: ['api', 'user', 'verify']
})

export default verifyEmail
