import verifyEmail from './email'
import verifyPhone from './phone'

export default {
  email: verifyEmail,
  phone: verifyPhone
}
