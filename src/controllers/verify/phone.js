import Boom from 'boom'
import Joi from 'joi'
import moment from 'moment'

import User from 'database/models/user'
import { createToken, omit } from 'utils'

const verifyPhone = ({
  auth: false,
  validate: {
    params: Joi.object().keys({
      user: Joi.string().required(),
      token: Joi.string().required()
    })
  },
  handler: (request, reply) => {
    User.findOne({
      _id: request.params.user,
      'phone.verifyToken': request.params.token
    }, `-password -__v`, (err, user) => {
      if (err) {
        return reply(Boom.wrap(err))
      }

      if (!user) {
        return reply(Boom.badRequest('error.invalidToken'))
      }

      if (moment().isAfter(user.phone.tokenExpiration)) {
        return reply(Boom.badRequest('error.expiredToken'))
      }

      const $set = {
        phone: {
          ...user.phone,
          verified: true,
          verifyToken: ''
        },
        updatedOn: Date.now()
      }

      User.findByIdAndUpdate(user._id, { $set }, (err) => {
        if (err) {
          return reply(Boom.wrap(err))
        }

        const userObject = omit(user.toObject(), ['password', 'email.verifyToken', 'phone.verifyToken', 'profilePicture'])

        userObject.scope = 'tmpuser'

        return reply({ token: createToken(userObject) })
      })
    })
  },
  description: `Verify user phone`,
  tags: ['api', 'user', 'verify']
})

export default verifyPhone
