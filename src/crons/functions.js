import fs from 'fs'
import Handlebars from 'handlebars'
import pdf from 'html-pdf'
import json2csv from 'json2csv'
import path from 'path'
import QRCode from 'qrcode-npm'

import { apiURL } from '../config'
import { sendDailyOrderExport, sendOrderNotCompletedReminder } from 'emails'
import Order from 'database/models/order'
import User from 'database/models/user' // eslint-disable-line
import { logger } from 'plugins/winston'
import { getFormattedDateTime } from 'utils'

export const remindOrderNotCompleted = (ses, number) => {
  logger.log('info', `CRON remindOrderNotCompleted started for order #${number}`)

  Order.findOne({ number })
    .populate('user')
    .exec((err, order) => {
      if (err) {
        return logger.log('error', `CRON remindOrderNotCompleted failed for order #${number}:`, err)
      }

      if (!order) {
        return logger.log('error', `CRON remindOrderNotCompleted failed for order #${number}: order not found`)
      }

      if (order.state === 'IN_PROCESS') {
        sendOrderNotCompletedReminder(ses, order, order.user).then(
          (response) => logger.log('info', `CRON remindOrderNotCompleted stopped for order #${number}: reminder email sent to ${order.user.email.email}`),
          (reason) => logger.log('error', `CRON remindOrderNotCompleted failed for order #${number}:`, reason)
        )
      } else {
        logger.log('info', `CRON remindOrderNotCompleted stopped for order #${number}: order already completed`)
      }
    })
}

export const dailyOrderExport = () => {
  logger.log('info', `CRON dailyOrderExport started`)

  const tomorrow = new Date()

  tomorrow.setDate(tomorrow.getDate() + 1)

  Order.find({ 'pickup.date': { $gt: new Date(), $lt: tomorrow } })
    .populate('user')
    .exec((err, orders) => {
      if (err) {
        return logger.log('error', `CRON dailyOrderExport failed:`, err)
      }

      orders = orders.reduce((acc, order) => {
        if (order.states[order.states.length - 1].id === 'ORDERED') {
          order = order.toObject()
          order.pickup.date = getFormattedDateTime(order.pickup.date)
          order.delivery.date = getFormattedDateTime(order.delivery.date)

          const flash = QRCode.qrcode(8, 'M')

          flash.addData(`${apiURL}/order/${order._id}/delivered`, 'Byte')
          flash.make()

          order.flash = flash.createImgTag(1)

          acc.push(order)
        }

        return acc
      }, [])

      const objectFields = ['number', 'user.firstname', 'user.lastname', 'user.phone.phone', 'pickup.address', 'pickup.date', 'delivery.date', 'delivery.address', 'luggages', 'flightNumber']
      const csvFields = ['#', 'Firstname', 'Lastname', 'Phone', 'Pickup address', 'Pickup date/time', 'Delivery address', 'Delivery date/time', 'Luggage quantity', 'Flight number']

      json2csv({
        data: orders,
        fields: objectFields,
        fieldNames: csvFields,
        quotes: '',
        del: ';'
      }, (err, csv) => {
        if (err) {
          return logger.log('error', `CRON dailyOrderExport failed to convert order data to CSV format: ${err}`)
        }

        fs.readFile(path.join(__dirname, '../html/daily-export.html'), 'utf8', (err, source) => {
          if (err) {
            return logger.log('error', `CRON dailyOrderExport failed to find source HTML file:`, err)
          }

          const template = Handlebars.compile(source)
          const html = template({ orders })

          const options = {
            format: 'A4',
            orientation: 'portrait',
            border: '1cm'
          }

          pdf.create(html, options).toBuffer((err, buffer) => {
            if (err) {
              return logger.log('error', `CRON dailyOrderExport failed to create PDF file:`, err)
            }

            const pdfContent = buffer.toString('base64')

            sendDailyOrderExport(pdfContent, csv).then(
              (response) => logger.log('info', `CRON dailyOrderExport stopped: daily export successfully sent`),
              (reason) => logger.log('error', `CRON dailyOrderExport failed:`, reason)
            )
          })
        })
      })
    })
}
