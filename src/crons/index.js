import { CronJob } from 'cron'

import { logger } from 'plugins/winston'
import { dailyOrderExport, remindOrderNotCompleted } from './functions'
import { getFormattedDateTime } from 'utils'

const noop = () => {}

export const orderNotCompletedCron = (ses, orderNumber, daysToSend) => {
  const cronTime = new Date()

  cronTime.setDate(cronTime.getDate() + daysToSend)

  const job = new CronJob(cronTime, remindOrderNotCompleted.bind(remindOrderNotCompleted, ses, orderNumber), noop, true) // eslint-disable-line no-unused-vars

  if (job && !job.running) {
    logger.log('info', `CRON remindOrderNotCompleted registered for order #${orderNumber} on ${getFormattedDateTime(cronTime)}`)
  }
}

export const orderExportCron = () => {
  // Preliminary CRON (16h but 18h in FR)
  const preliminaryOrderExportJob = new CronJob('00 00 16 * * 0-6', dailyOrderExport, noop, true) // eslint-disable-line no-unused-vars
  // Final CRON (20h but 22h in FR)
  const finalOrderExportJob = new CronJob('00 00 20 * * 0-6', dailyOrderExport, noop, true) // eslint-disable-line no-unused-vars
}
