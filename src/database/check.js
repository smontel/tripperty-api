import Promise from 'bluebird'

import Admin from 'database/models/admin'
import Product from 'database/models/price'
import { hashPassword } from 'utils'

const checkDB = () => {
  Promise.all([
    checkAdmin(),
    checkProducts()
  ]).catch((err) => {
    throw new Error(err)
  })
}

const checkAdmin = () => new Promise((resolve, reject) => {
  Admin.find({}, (err, admins) => {
    if (err) {
      return reject(`Couldn’t query Admin collection: ${err}`)
    }

    if (admins.length === 0) {
      hashPassword('test', (err, hash) => {
        if (err) {
          return reject(`Couldn’t generate default admin password: ${err}`)
        }

        const admin = new Admin({
          login: 'default-admin',
          password: hash
        })

        admin.save((err) => {
          if (err) {
            return reject(`Couldn’t create default admin: ${err}`)
          }

          return resolve()
        })
      })
    }
  })
})

const checkProducts = () => new Promise((resolve, reject) => {
  Product.find({}, (err, products) => {
    if (err) {
      return reject(`Couldn’t query Product collection: ${err}`)
    }

    const defaultProducts = [{
      product: 'default',
      price: 30,
      maxBundle: 3
    }, {
      product: 'extra',
      price: 10
    }, {
      product: 'storage',
      price: 5
    }, {
      product: 'registrationZoneExtra',
      price: 5
    }, {
      product: 'registrationZone',
      price: 15
    }]

    if (products.length > 0) {
      products.map((product) => {
        const index = defaultProducts.reduce((acc, defaultProduct, i) => {
          if (acc === -1 && defaultProduct.product === product.product) {
            return i
          }

          return acc
        }, -1)

        if (index !== -1) {
          defaultProducts.splice(index, 1)
        }
      })
    }

    if (defaultProducts.length !== 0) {
      Product.insertMany(defaultProducts, (err) => {
        if (err) {
          return reject(`Couldn’t create default products: ${err}`)
        }

        return resolve()
      })
    }
  })
})

export default checkDB
