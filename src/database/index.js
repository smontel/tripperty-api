import Promise from 'bluebird'
import mongoose from 'mongoose'

import { mongo } from '../config'
import { logger } from 'plugins/winston'
import { getFormattedDateTime } from 'utils/index'

const mongooseOptions = {
  server: {
    reconnectTries: 10,
    reconnectInterval: 60
  }
}

if (process.env.ENV === 'production') {
  mongooseOptions.server.reconnectTries = Number.MAX_VALUE
  mongooseOptions.server.reconnectInterval = 5
}

mongoose.connect(`mongodb://${mongo.user}:${mongo.pwd}@${mongo.host}`, mongooseOptions)
mongoose.Promise = Promise

const db = mongoose.connection
const commonText = `to ${mongo.host}`

db.on('error', (err) => logger.log('error', `${getFormattedDateTime()} : MongoDB connection failed ${commonText} : ${err}`))
db.once('open', function () {
  logger.log('info', `${getFormattedDateTime()} : MongoDB connected ${commonText}`)
})

export {
  mongoose,
  db
}
