import { mongoose } from '..'

const adminSchema = new mongoose.Schema({
  login: {
    type: String,
    required: true,
    index: true
  },
  password: {
    type: String,
    required: true
  },
  scope: {
    type: String,
    default: 'admin',
    enum: ['admin']
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
})

export default mongoose.model('Admin', adminSchema)
