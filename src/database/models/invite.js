import { mongoose } from '..'

const inviteSchema = new mongoose.Schema({
  sponsor: {
    type: String,
    required: true,
    ref: 'User'
  },
  invitee: {
    type: String,
    required: true,
    index: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  }
})

export default mongoose.model('Invite', inviteSchema)
