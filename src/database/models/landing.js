import { mongoose } from '..'

const landingSchema = mongoose.Schema({
  email: {
    type: String,
    required: true,
    index: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
})

export default mongoose.model('Landing', landingSchema)
