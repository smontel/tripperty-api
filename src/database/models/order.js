import { mongoose } from '..'

const orderSchema = mongoose.Schema({
  number: {
    type: String,
    index: true,
    required: true
  },
  pickup: {
    address: {
      type: String,
      required: true
    },
    date: {
      type: Date,
      required: true
    }
  },
  delivery: {
    address: {
      type: String
    },
    latitude: {
      type: String,
      default: '49.009691'
    },
    longitude: {
      type: String,
      default: '2.547925'
    },
    date: {
      type: Date,
      required: true
    }
  },
  luggages: {
    type: Number,
    required: true
  },
  storage: {
    type: Number,
    default: 0
  },
  isCgvChecked: {
    type: Boolean,
    required: true
  },
  isToggledRegistrationZone: {
    type: Boolean
  },
  price: {
    total: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },

    includedLuggage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    extraLuggage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    storage: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    registrationZone: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    registrationZoneExtra: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    },
    discount: {
      ht: {
        type: Number
      },
      tva: {
        type: Number
      },
      ttc: {
        type: Number
      }
    }
  },
  stripeCharge: {
    type: String,
    default: ''
  },
  stripeRefund: {
    type: String,
    default: ''
  },
  stripeId: {
    type: String
  },
  cardLast4: {
    type: String
  },
  cardBrand: {
    type: String
  },
  cardId: {
    type: String
  },
  states: [{
    id: {
      type: String,
      enum: [
        'IN_PROCESS',
        'ORDERED',
        'WAIT_FOR_PICKUP',
        'IN_TRANSIT',
        'ARRIVED',
        'DELIVERED',
        'ANOMALY',
        'REFUNDED'
      ]
    },
    date: {
      type: Date
    },
    eta: {
      type: Date
    }
  }],
  flightNumber: {
    type: String,
    default: ''
  },
  user: {
    type: String,
    required: true,
    ref: 'User'
  },
  rating: {
    type: Number,
    default: 0
  },
  customerTimeArrival: {
    type: Date
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
})

orderSchema.virtual('state').get(function () {
  return this.states ? this.states[this.states.length - 1].id : null
})

orderSchema.pre('save', function(next) {
  if (this.isToggledRegistrationZone === true) {
    this.delivery.address = 'Aeroport CDG - RDV sur ma zone d’enregistrement';
  } else {
    this.delivery.address = 'Baggages du Monde – CDG2 – Gare TGV';
  }

  next();
});

orderSchema.set('toObject', { virtuals: true })
orderSchema.set('toJSON', { virtuals: true })

export default mongoose.model('Order', orderSchema)
