import { mongoose } from '..'

const productSchema = mongoose.Schema({
  product: {
    type: String,
    required: true,
    index: true
  },
  price: {
    type: Number,
    required: true
  },
  maxBundle: {
    type: Number
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
})

export default mongoose.model('Price', productSchema)
