import moment from 'moment'

import { mongoose } from '..'
import { randomString, uuid } from 'utils'

const userSchema = mongoose.Schema({
  providerId: {
    type: String,
    index: true
  },
  firstname: {
    type: String,
    required: true
  },
  lastname: {
    type: String,
    required: true
  },
  profilePicture: {
    type: String
  },
  password: {
    type: String
  },
  email: {
    email: {
      type: String,
      required: true,
      index: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    verifyToken: {
      type: String,
      default: () => uuid()
    },
    resetToken: String,
    tokenExpiration: {
      type: Date,
      default: () => moment().add(1, 'day')
    },
    markedAsSpam: {
      type: Boolean,
      default: false
    }
  },
  country: {
    value: String,
    label: String,
    prefix: Number,
  },
  phone: {
    countryCode: {
      type: String
    },
    phone: {
      type: String,
      required: true,
      index: true
    },
    international: {
      type: String,
      required: true,
      index: true
    },
    verified: {
      type: Boolean,
      default: false
    },
    verifyToken: {
      type: String,
      default: () => randomString(6)
    },
    tokenExpiration: {
      type: Date,
      default: () => moment().add(1, 'day')
    },
    regenCount: {
      type: Number,
      default: 0
    },
    lastRegen: Date
  },
  billing: {
    firstname: String,
    lastname: String,
    company: String,
    address: String, // split ?
    vat: String
  },
  sponsor: {
    type: String,
    ref: 'User'
  },
  sponsorRewarded: {
    type: Boolean,
    default: false
  },
  accountBalance: {
    type: Number,
    default: 0
  },
  stripeId: {
    type: String,
    default: ''
  },
  playerIds: [{
    type: String
  }],
  language: {
    type: String,
    enum: ['fr', 'en'],
    required: true
  },
  scope: {
    type: String,
    default: 'user',
    enum: ['user']
  },
  active: {
    type: Boolean,
    default: true
  },
  notification: {
    type: Boolean,
    default: true
  },
  createdOn: {
    type: Date,
    default: () => Date.now()
  },
  updatedOn: Date
})

userSchema.virtual('fullname').get(function () {
  return `${this.firstname} ${this.lastname}`
})

userSchema.set('toObject', { virtuals: true })
userSchema.set('toJSON', { virtuals: true })

export default mongoose.model('User', userSchema)
