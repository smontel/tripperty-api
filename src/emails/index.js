import AWS from 'aws-sdk'
import Promise from 'bluebird'
import { mjml2html } from 'mjml'

import { aws, defaultIdentity } from '/config'
import Price from 'database/models/price'
import { getFormattedDateTime, getTranslations } from '/utils'

const FROM = defaultIdentity
const REPLYTO = ['no-reply@tripperty.com']
const hosts = {
  api: process.env.API_URL,
  front: process.env.FRONT_URL
}

const buildEmail = (destination, subject, body) => ({
  Source: FROM,
  ReplyToAddresses: REPLYTO,
  Destination: {
    ToAddresses: destination
  },
  Message: {
    Subject: {
      Data: subject
    },
    Body: {
      Html: {
        Data: body
      }
    }
  }
})

const sendEmail = (email, ses) => new Promise((resolve, reject) => ses.sendEmail(email, (err, data) => {
  if (err) {
    return reject(err)
  }

  resolve(data)
}))

export const sendEmailValidation = (ses, user) => new Promise((resolve, reject) => {
  if (user.email.markedAsSpam) {
    return reject('You marked at least one Tripperty email as spam, please contact our support.')
  }

  const body = mjml2html(`
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0" font-family="Arial" align="center" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-container>

      <!-- Header -->
      <mj-section background-color="#FFFFFF" padding-top="70px" padding-bottom="0">
        <mj-column width="600px">
          <mj-image width="600px" src="${hosts.api}/assets/images/logo_tripperty.png" alt="Tripperty Logo" title="Tripperty Logo" padding-bottom="75px" />
          <mj-image width="600px" src="${hosts.api}/assets/images/conseiller.png" alt="Conseiller Tripperty" title="Conseiller Tripperty" padding-bottom="30px" />
          <mj-text color="#29363C" font-size="14px" font-weight="700" letter-spacing="2px" padding-bottom="30px">${getTranslations('email.global.trippertyAdvisor')[user.language]}</mj-text>
          <mj-image width="50px" src="${hosts.api}/assets/images/triangle.png" />
        </mj-column>
      </mj-section>

      <!-- Message -->
      <mj-section background-color="#29363C" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-size="22px" font-weight="700" line-height="30px" padding="0 30px 30px">
            ${getTranslations('email.global.hello')[user.language]} ${user.fullname},
            <span style="color: #9AB999">${getTranslations('email.emailValidation.highlighted')[user.language]}</span>
            ${getTranslations('email.emailValidation.rest')[user.language]}
          </mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 1 -->
      <mj-section background-color="#FFFFFF" padding="0">
        <mj-column width="50%">
          <mj-image height="30px" width="22px" src="${hosts.api}/assets/images/lock.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.security')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.securityText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/cancel.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.cancel')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.cancelText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 2 -->
      <mj-section background-color="#FFFFFF" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="20px" src="${hosts.api}/assets/images/money.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.insurance')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.insuranceText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/phone_call.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.afterSale')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.afterSaleText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-top="35px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="24px" font-weight="700" padding="0">${getTranslations('email.global.needHelp')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="12px" src="${hosts.api}/assets/images/phone.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.byPhone')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.phoneNumber')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/chat.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.onWhatsapp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.whatsappNumber')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

    </mj-container>
  </mj-body>
</mjml>`)

  if (!body.html) {
    return reject(body.errors)
  }

  sendEmail(buildEmail([user.email.email], getTranslations('email.emailValidation.subject')[user.language], body.html), ses).then(
    (response) => resolve(response),
    (reason) => reject(reason)
  )
})

export const sendPasswordReset = (ses, user) => new Promise((resolve, reject) => {
  if (user.email.markedAsSpam) {
    return reject('You marked at least one Tripperty email as spam, please contact our support.')
  }

  const body = mjml2html(`
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0" font-family="Arial" align="center" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-container>

      <!-- Header -->
      <mj-section background-color="#FFFFFF" padding-top="70px" padding-bottom="0">
        <mj-column width="600px">
          <mj-image width="600px" src="${hosts.api}/assets/images/logo_tripperty.png" alt="Tripperty Logo" title="Tripperty Logo" padding-bottom="75px" />
          <mj-image width="600px" src="${hosts.api}/assets/images/conseiller.png" alt="Conseiller Tripperty" title="Conseiller Tripperty" padding-bottom="30px" />
          <mj-text color="#29363C" font-size="14px" font-weight="700" letter-spacing="2px" padding-bottom="30px">${getTranslations('email.global.trippertyAdvisor')[user.language]}</mj-text>
          <mj-image width="50px" src="${hosts.api}/assets/images/triangle.png" />
        </mj-column>
      </mj-section>

      <!-- Message -->
      <mj-section background-color="#29363C" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-size="22px" font-weight="700" line-height="30px" padding="0 30px 30px">
            ${getTranslations('email.global.hello')[user.language]} ${user.fullname},
            <span style="color: #9AB999">${getTranslations('email.passwordReset.highlighted')[user.language]}</span>
            ${getTranslations('email.passwordReset.rest')[user.language]}
          </mj-text>
          <mj-button background-color="#9AB999" border-radius="500px" font-size="13px" font-weight="700" href="${hosts.front}?action=reset&token=${user.email.resetToken}" inner-padding="20px 45px">
            <span style="letter-spacing: 2px;">${getTranslations('email.passwordReset.action')[user.language]}</span>
          </mj-button>
          <mj-text color="#F6F7F7" font-size="16px" padding="20px 30px 0" line-height="1.4">
            ${getTranslations('email.passwordReset.additionnalText')[user.language]}<a href="/todo" style="color: #9AB999;text-decoration: underline">${getTranslations('email.passwordReset.contactText')[user.language]}</a>.
          </mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 1 -->
      <mj-section background-color="#FFFFFF" padding="0">
        <mj-column width="50%">
          <mj-image height="30px" width="22px" src="${hosts.api}/assets/images/lock.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.security')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.securityText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/cancel.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.cancel')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.cancelText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 2 -->
      <mj-section background-color="#FFFFFF" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="20px" src="${hosts.api}/assets/images/money.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.insurance')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.insuranceText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/phone_call.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.afterSale')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.afterSaleText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-top="35px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="24px" font-weight="700" padding="0">${getTranslations('email.global.needHelp')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="12px" src="${hosts.api}/assets/images/phone.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.byPhone')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.phoneNumber')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/chat.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.onWhatsapp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.whatsappNumber')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

    </mj-container>
  </mj-body>
</mjml>`)

  if (body.errors.length > 0) {
    return reject(body.errors)
  }

  sendEmail(buildEmail([user.email.email], getTranslations('email.passwordReset.subject')[user.language], body.html), ses).then(
    (response) => resolve(response),
    (reason) => reject(reason)
  )
})

export const sendOrderNotCompletedReminder = (ses, order, user) => new Promise((resolve, reject) => {
  if (user.email.markedAsSpam) {
    return reject('User marked at least one Tripperty email as spam.')
  }

  const body = mjml2html(`
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0" font-family="Arial" align="center" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-container>

      <!-- Header -->
      <mj-section background-color="#FFFFFF" padding-top="70px" padding-bottom="0">
        <mj-column width="600px">
          <mj-image width="600px" src="${hosts.api}/assets/images/logo_tripperty.png" alt="Tripperty Logo" title="Tripperty Logo" padding-bottom="75px" />
          <mj-image width="600px" src="${hosts.api}/assets/images/conseiller.png" alt="Conseiller Tripperty" title="Conseiller Tripperty" padding-bottom="30px" />
          <mj-text color="#29363C" font-size="14px" font-weight="700" letter-spacing="2px" padding-bottom="30px">${getTranslations('email.global.trippertyAdvisor')[user.language]}</mj-text>
          <mj-image width="50px" src="${hosts.api}/assets/images/triangle.png" />
        </mj-column>
      </mj-section>

      <!-- Message -->
      <mj-section background-color="#29363C" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-size="22px" font-weight="700" line-height="30px" padding="0 30px 30px">
            ${getTranslations('email.global.hello')[user.language]} ${user.fullname},
            <span style="color: #9AB999">${getTranslations('email.orderReminder.highlighted')[user.language]}</span>
            ${getTranslations('email.orderReminder.rest')[user.language]}
          </mj-text>
          <mj-button background-color="#9AB999" border-radius="500px" font-size="13px" font-weight="700" href="/todo" inner-padding="20px 45px">
            <span style="letter-spacing: 2px;">${getTranslations('email.orderReminder.action')[user.language]}</span>
          </mj-button>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 1 -->
      <mj-section background-color="#FFFFFF" padding="0">
        <mj-column width="50%">
          <mj-image height="30px" width="22px" src="${hosts.api}/assets/images/lock.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.security')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.securityText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/cancel.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.cancel')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.cancelText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 2 -->
      <mj-section background-color="#FFFFFF" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="20px" src="${hosts.api}/assets/images/money.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.insurance')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.insuranceText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/phone_call.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.afterSale')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.afterSaleText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-top="35px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="24px" font-weight="700" padding="0">${getTranslations('email.global.needHelp')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="12px" src="${hosts.api}/assets/images/phone.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.byPhone')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.phoneNumber')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/chat.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.onWhatsapp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.whatsappNumber')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

    </mj-container>
  </mj-body>
</mjml>`)

  if (body.errors.length > 0) {
    return reject(body.errors)
  }

  sendEmail(buildEmail([user.email.email], getTranslations('email.orderReminder.subject')[user.language], body.html), ses).then(
    (response) => resolve(response),
    (reason) => reject(reason)
  )
})

export const sendInvite = (ses, user, invitee) => new Promise((resolve, reject) => {
  if (user.email.markedAsSpam) {
    return reject('User marked at least one Tripperty email as spam.')
  }

  const body = mjml2html(`
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0" font-family="Arial" align="center" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-container>

      <!-- Header -->
      <mj-section background-color="#FFFFFF" padding-top="70px" padding-bottom="0">
        <mj-column width="600px">
          <mj-image width="600px" src="${hosts.api}/assets/images/logo_tripperty.png" alt="Tripperty Logo" title="Tripperty Logo" padding-bottom="75px" />
          <mj-image width="600px" src="${hosts.api}/assets/images/conseiller.png" alt="Conseiller Tripperty" title="Conseiller Tripperty" padding-bottom="30px" />
          <mj-text color="#29363C" font-size="14px" font-weight="700" letter-spacing="2px" padding-bottom="30px">${getTranslations('email.global.trippertyAdvisor')[user.language]}</mj-text>
          <mj-image width="50px" src="${hosts.api}/assets/images/triangle.png" />
        </mj-column>
      </mj-section>

      <!-- Message -->
      <mj-section background-color="#29363C" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-size="22px" font-weight="700" line-height="30px" padding="0 30px 30px">
            ${getTranslations('email.global.hello')[user.language]},
            <span style="color: #9AB999">${user.firstname} ${getTranslations('email.invitation.highlighted')[user.language]}</span>
            ${getTranslations('email.invitation.rest')[user.language]}
          </mj-text>
          <mj-button background-color="#9AB999" border-radius="500px" font-size="13px" font-weight="700" href="/todo" inner-padding="20px 45px">
            <span style="letter-spacing: 2px;">${getTranslations('email.invitation.action')[user.language]}</span>
          </mj-button>
        </mj-column>
      </mj-section>

      <!-- Additionnal message -->
      <mj-section background-color="#F4F4F4" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="18px" font-weight="700" line-height="25px" padding="0 30px 20px">
            ${getTranslations('email.invitation.additionnalTextTitle')[user.language]}
          </mj-text>
          <mj-text color="#29363C" font-size="16px" line-height="25px" padding="0 30px">
            ${getTranslations('email.invitation.additionnalText')[user.language]}
          </mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 1 -->
      <mj-section background-color="#FFFFFF" padding="0">
        <mj-column width="50%">
          <mj-image height="30px" width="22px" src="${hosts.api}/assets/images/lock.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.security')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.securityText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/cancel.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.cancel')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.cancelText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 2 -->
      <mj-section background-color="#FFFFFF" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="20px" src="${hosts.api}/assets/images/money.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.insurance')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.insuranceText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/phone_call.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.global.afterSale')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.global.afterSaleText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-top="35px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="24px" font-weight="700" padding="0">${getTranslations('email.global.needHelp')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="12px" src="${hosts.api}/assets/images/phone.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.byPhone')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.phoneNumber')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/chat.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.onWhatsapp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.whatsappNumber')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

    </mj-container>
  </mj-body>
</mjml>`)

  if (body.errors.length > 0) {
    return reject(body.errors)
  }

  sendEmail(buildEmail([invitee], getTranslations('email.invitation.subject')[user.language], body.html), ses).then(
    (response) => resolve(response),
    (reason) => reject(reason)
  )
})

export const sendOrderConfirmation = (ses, order, user) => new Promise((resolve, reject) => {
  if (user.email.markedAsSpam) {
    return reject('You marked at least one Tripperty email as spam, please contact our support.')
  }

  Price.findOne({ product: 'default' }, (err, price) => {
    if (err) {
      return reject(err)
    }

    const maxBundle = price.maxBundle
    let extras = ''

    order.payedOn = getFormattedDateTime(order.states.filter((state) => state.id === 'ORDERED')[0].date, 'd mmmm yyyy')

    if (order.price.extraLuggage && order.price.extraLuggage.ttc) {
      extras += `<mj-text color="#F6F7F7" font-weight="700" font-size="15px" line-height="20px">${getTranslations('email.orderSummary.extraLuggage')[user.language]} <span style="padding-left: 10px;color: #9AB999">x${order.luggages - maxBundle}</span> <span style="color: #7B8990">+${order.price.extraLuggage.ttc}€</span></mj-text>`
    }

    if (order.price.storage && order.price.storage.ttc) {
      extras += `<mj-text color="#F6F7F7" font-weight="700" font-size="15px" line-height="20px">${getTranslations('email.orderSummary.storage')[user.language]} <span style="padding-left: 10px;color: #9AB999">x${order.storage}</span> <span style="color: #7B8990">+${order.price.storage.ttc}€</span></mj-text>`
    }

    if (order.price.discount && order.price.discount.ttc) {
      extras += `<mj-text color="#F6F7F7" font-weight="700" font-size="15px" line-height="20px">${getTranslations('email.orderSummary.discount')[user.language]} <span style="padding-left: 10px;color: #7B8990">-${order.price.discount.ttc}€</span></mj-text>`
    }
    if (order.price.registrationZone && order.price.registrationZone.ttc) {
      extras += `<mj-text color="#F6F7F7" font-weight="700" font-size="15px" line-height="20px">${getTranslations('email.orderSummary.registrationZone')[user.language]} <span style="color: #7B8990">+${order.price.registrationZone.ttc}€</span></mj-text>`
    }
    const body = mjml2html(`
<mjml>
  <mj-head>
    <mj-attributes>
      <mj-all padding="0" font-family="Arial" align="center" />
    </mj-attributes>
  </mj-head>
  <mj-body>
    <mj-container>

      <!-- Header -->
      <mj-section background-color="#FFFFFF" padding-top="70px" padding-bottom="0">
        <mj-column width="600px">
          <mj-image width="600px" src="${hosts.api}/assets/images/logo_tripperty.png" alt="Tripperty Logo" title="Tripperty Logo" padding-bottom="75px" />
          <mj-image width="600px" src="${hosts.api}/assets/images/conseiller.png" alt="Conseiller Tripperty" title="Conseiller Tripperty" padding-bottom="30px" />
          <mj-text color="#29363C" font-size="14px" font-weight="700" letter-spacing="2px" padding-bottom="30px">${getTranslations('email.global.trippertyAdvisor')[user.language]}</mj-text>
          <mj-image width="50px" src="${hosts.api}/assets/images/triangle.png" />
        </mj-column>
      </mj-section>

      <!-- Message -->
      <mj-section background-color="#29363C" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-size="22px" font-weight="700" line-height="30px" padding="0 30px 30px">
            ${getTranslations('email.global.hello')[user.language]} ${user.fullname},
            <span style="color: #9AB999">${getTranslations('email.orderSummary.highlighted')[user.language]}</span>
            ${getTranslations('email.orderSummary.rest')[user.language]}
          </mj-text>
        </mj-column>
      </mj-section>

      <!-- Order summary -->
      <mj-section background-color="#374951" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#F6F7F7" font-weight="700" font-size="18px">${getTranslations('email.orderSummary.orderSummary')[user.language]}</mj-text>
          <mj-text color="#F6F7F7" font-size="15px"># ${order.number}</mj-text>
          <mj-text color="#7B8990" font-size="15px">${getTranslations('email.orderSummary.orderedOn')[user.language]} ${order.payedOn}</mj-text>
          <mj-text color="#9AB999" font-size="40px" font-weight="700" padding="25px 0 15px">${order.price.total.ttc}€</mj-text>
          <mj-text color="#F6F7F7" font-weight="700" font-size="15px" line-height="20px">${getTranslations('email.orderSummary.luggageTransfert')[user.language]} <span style="padding-left: 10px;color: #7B8990">${order.price.includedLuggage.ttc}€</span></mj-text>
          ${extras}
          <mj-text color="#9AB999" font-weight="700" font-size="18px" padding="20px 0 10px">${getTranslations('email.orderSummary.departure')[user.language]}</mj-text>
          <mj-text color="#F6F7F7" font-size="15px" line-height="20px">${getFormattedDateTime(order.pickup.date)}</mj-text>
          <mj-text color="#F6F7F7" font-size="15px" line-height="20px">${order.pickup.address}</mj-text>
          <mj-text color="#F4837D" font-weight="700" font-size="18px" padding="20px 0 10px">${getTranslations('email.orderSummary.arrival')[user.language]}</mj-text>
          <mj-text color="#F6F7F7" font-size="15px" line-height="20px">${getFormattedDateTime(order.delivery.date)}</mj-text>
          <mj-text color="#F6F7F7" font-size="15px" line-height="20px">${order.delivery.address}</mj-text>
          <mj-button background-color="#9AB999" border-radius="500px" font-size="13px" font-weight="700" href="/todo" inner-padding="20px 45px" padding-top="30px">
            <span style="letter-spacing: 2px;">${getTranslations('email.orderSummary.action')[user.language]}</span>
          </mj-button>
          <mj-text color="#F6F7F7" font-size="16px" padding="20px 30px 0" line-height="1.4">
            ${getTranslations('email.orderSummary.addEvent')[user.language]} <a href="${hosts.api}/todo" style="color: #9AB999;text-decoration: underline">${getTranslations('email.orderSummary.linkText')[user.language]}</a>
          </mj-text>
        </mj-column>
      </mj-section>

      <!-- Additionnal text -->
      <mj-section background-color="#F4F4F4" padding-top="45px" padding-bottom="40px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="18px" font-weight="700" line-height="25px" padding="0 30px">
            ${getTranslations('email.orderSummary.additionnalText')[user.language]}
          </mj-text>
          <mj-image width="600px" src="${hosts.api}/assets/images/tripperty_for_ios-fr.png" padding="50px 0" />
          <mj-button background-color="#F4837D" border-radius="500px" font-size="13px" font-weight="700" href="${hosts.api}/todo" inner-padding="20px 45px">
            <span style="letter-spacing: 2px;">${getTranslations('email.orderSummary.downloadText')[user.language]}</span>
          </mj-button>
        </mj-column>
      </mj-section>

      <!-- FAQ ROW 1 -->
      <mj-section background-color="#FFFFFF" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="17px" src="${hosts.api}/assets/images/luggage.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.orderSummary.luggages')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.orderSummary.luggagesText')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/help.png" padding-top="50px" />
          <mj-text color="#29363C" font-size="20px" font-weight="700" line-height="40px">${getTranslations('email.orderSummary.onlineHelp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" padding="0 20px" line-height="20px">${getTranslations('email.orderSummary.onlineHelpText')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-top="35px">
        <mj-column width="100%">
          <mj-text color="#29363C" font-size="24px" font-weight="700" padding="0">${getTranslations('email.global.needHelp')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

      <mj-section background-color="#F4F4F4" padding-bottom="50px">
        <mj-column width="50%">
          <mj-image height="30px" width="12px" src="${hosts.api}/assets/images/phone.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.byPhone')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.phoneNumber')[user.language]}</mj-text>
        </mj-column>
        <mj-column width="50%">
          <mj-image height="30px" width="30px" src="${hosts.api}/assets/images/chat.png" padding-top="30px" />
          <mj-text color="#29363C" font-size="12px" letter-spacing="2px" line-height="40px">${getTranslations('email.global.onWhatsapp')[user.language]}</mj-text>
          <mj-text color="#29363C" font-size="18px" font-weight="700" padding="0 20px">${getTranslations('email.global.whatsappNumber')[user.language]}</mj-text>
        </mj-column>
      </mj-section>

    </mj-container>
  </mj-body>
</mjml>`)

    if (body.errors.length > 0) {
      return reject(body.errors)
    }

    sendEmail(buildEmail([user.email.email], getTranslations('email.orderSummary.subject')[user.language], body.html), ses).then(
      (response) => resolve(response),
      (reason) => reject(reason)
    )
  })
})

export const sendDailyOrderExport = (pdf, csv) => new Promise((resolve, reject) => {
  const SES = new AWS.SES({
    region: 'us-west-2',
    credentials: aws
  })

  const filename = `${getFormattedDateTime(new Date(), 'yyyy-mm-dd')}__tripperty-order-export`
  // const from = `'Tripperty Server' <${replyTo[0]}>`
  const sendTo = process.env.ORDER_EXPORT_RECIPIENTS
  const mail = `From: <${defaultIdentity}>
Subject: Tripperty daily order export
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="NextPart"

--NextPart
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 7bit

See attachment.

--NextPart
Content-Type: application/octet-stream; name="${filename}.pdf"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="${filename}.pdf"

${pdf}

--NextPart
Content-Type: text/csv; name="${filename}.csv"; charset=utf-8
Content-Disposition: attachment; filename="${filename}.csv"

${csv}

--NextPart`
  const options = {
    RawMessage: { Data: new Buffer(mail) },
    Destinations: sendTo.split(','),
    Source: `<${defaultIdentity}>`
  }

  SES.sendRawEmail(options, (err, response) => {
    if (err) {
      return reject(err)
    }

    return resolve(response)
  })
})

export const sendOrderInvoice = (pdf, order) => new Promise((resolve, reject) => {
  const SES = new AWS.SES({
    region: 'us-west-2',
    credentials: aws
  })

  const filename = `INVOICE__${order.number}__TRIPPERTY_BAG.pdf`
  const sendTo = order.user.email.email
  const mail = `From: <${defaultIdentity}>
Subject: Tripperty invoice #${order.number}
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary="NextPart"

--NextPart
Content-Type: text/html; charset=utf-8
Content-Transfer-Encoding: 7bit

See attachment.

--NextPart
Content-Type: application/octet-stream; name="${filename}"
Content-Transfer-Encoding: base64
Content-Disposition: attachment; filename="${filename}"

${pdf}

--NextPart`
  const options = {
    RawMessage: { Data: new Buffer(mail) },
    Destinations: [sendTo],
    Source: `<${defaultIdentity}>`
  }

  SES.sendRawEmail(options, (err, response) => {
    if (err) {
      return reject(err)
    }

    return resolve(response)
  })
})
