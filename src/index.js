import Hapi from 'hapi'

import AuthStategy from 'auth/strategies'
import { orderExportCron } from 'crons'
import checkDB from 'database/check'
import Plugins from 'plugins'
import Routes from 'routes'
import Relish from 'routes-validation'
import { getFormattedDateTime } from 'utils'

let serverOptions = {}

if (process.env.ENV !== 'production') {
  serverOptions.debug = {
    request: ['error']
  }
}

const server = new Hapi.Server(serverOptions)

server.connection({
  port: process.env.PORT || 3100,
  routes: {
    cors: true,
    validate: {
      failAction: Relish.failAction
    }
  },
  router: {
    stripTrailingSlash: true
  }
})

// Register plugins
server.register(Plugins, (err) => {
  if (err) {
    throw err
  }

  // Auth stategy
  AuthStategy(server)

  // Plugins to expose
  server.app.logger = server.plugins['hapi-winston'].logger
  server.app.getPhoneInfo = server.plugins['hapi-twilio-lookup'].getPhoneInfo
  server.app.aws = server.plugins['aws'].aws
  server.app.stripe = server.plugins['hapi-stripe'].stripe
  server.app.oneSignal = server.plugins['hapi-onesignal-notification']

  // Database check
  checkDB()

  // Daily export CRON
  orderExportCron()

  // Routes
  server.route(Routes)

  // Start
  server.start((err) => {
    if (err) {
      throw err
    }

    server.app.logger.log('info', `${getFormattedDateTime()} : Server started at ${server.info.uri}`)
  })
})
