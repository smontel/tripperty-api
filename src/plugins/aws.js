import AWS from 'aws-sdk'

import { aws, defaultIdentity, snsTopic as SnsTopic, snsRegion } from '/config'

const register = (server, options, next) => {
  const o = {
    region: snsRegion,
    ...options,
    credentials: aws
  }
  const ses = new AWS.SES(o)

  ses.setIdentityNotificationTopic({
    Identity: defaultIdentity,
    NotificationType: 'Delivery',
    SnsTopic
  }, (err, data) => {
    if (err) {
      throw err
    }
  })

  ses.setIdentityNotificationTopic({
    Identity: defaultIdentity,
    NotificationType: 'Bounce',
    SnsTopic
  }, (err, data) => {
    if (err) {
      throw err
    }
  })

  ses.setIdentityNotificationTopic({
    Identity: defaultIdentity,
    NotificationType: 'Complaint',
    SnsTopic
  }, (err, data) => {
    if (err) {
      throw err
    }
  })

  server.expose('aws', {
    ses,
    sns: new AWS.SNS(o)
  })

  next()
}

register.attributes = {
  name: 'aws',
  version: '1.0.0'
}

export default {
  register
}
