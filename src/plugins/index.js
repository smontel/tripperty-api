import JWT from 'hapi-auth-jwt'
import Inert from 'inert'
import Vision from 'vision'

import AWS from './aws'
import Lout from './lout'
import OneSignal from './onesignal-notification'
import Stripe from './stripe'
import TwilioLookup from './twilio-lookup'
import Winston from './winston'

const plugins = [
  AWS,
  Inert,
  JWT,
  Lout,
  OneSignal,
  Stripe,
  TwilioLookup,
  Vision,
  Winston
]

export default plugins
