import Promise from 'bluebird'
import https from 'https'

import { oneSignalAppId, oneSignalRestApiKey } from '/config'
import { getTranslations } from 'utils'

const register = (server, o, next) => {
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `Basic ${oneSignalRestApiKey}`
  }

  const options = {
    host: 'onesignal.com',
    port: 443,
    path: '/api/v1/notifications',
    method: 'POST',
    headers: headers
  }

  const createNotification = (sendTo, title, content, data = {}) => ({
    app_id: oneSignalAppId,
    headings: getTranslations(title),
    contents: getTranslations(content),
    data,
    include_player_ids: sendTo,
    ios_badgeType: 'Increase',
    ios_badgeCount: 1
  })

  const sendNotification = (notification) => new Promise((resolve, reject) => {
    const req = https.request(options, (res) => {
      res.on('data', (data) => {
        let response

        try {
          response = JSON.parse(data)

          if (response.hasOwnProperty('errors')) {
            return reject(response.errors)
          }

          return resolve(response)
        } catch (err) {
          return reject(err)
        }
      })
    })

    req.on('error', (err) => reject(err))
    req.write(JSON.stringify(notification))
    req.end()
  })

  server.expose('sendNotification', sendNotification)
  server.expose('createNotification', createNotification)

  next()
}

register.attributes = {
  name: 'hapi-onesignal-notification',
  version: '1.0.0'
}

export default {
  register
}
