import stripe from 'stripe'

import { stripe as stripeConfig } from '/config'

const register = (server, options, next) => {
  server.expose('stripe', stripe(stripeConfig))

  next()
}

register.attributes = {
  name: 'hapi-stripe',
  version: '1.0.0'
}

export default {
  register
}
