import Promise from 'bluebird'
import Twilio from 'twilio'

import { twilio } from '/config'

const register = (server, options, next) => {
  const client = new Twilio.LookupsClient(twilio.sid, twilio.authToken)

  const getPhoneInfo = (phone, options) => new Promise((resolve, reject) => {
    if (typeof phone !== 'string') {
      return reject(`${phone} is not a string`)
    }

    phone = phone.replace(/\D+/g, '') // eslint-disable-line

    client.phoneNumbers(phone).get(options || null, (err, data) => {
      if (err) {
        return reject(err)
      }

      return resolve(data)
    })
  })

  server.expose('getPhoneInfo', getPhoneInfo)

  next()
}

register.attributes = {
  name: 'hapi-twilio-lookup',
  version: '1.0.0'
}

export default {
  register
}
