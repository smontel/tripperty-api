import path from 'path'
import winston from 'winston'

const root = path.join(__dirname, '..')

const createLogger = (logPath = '') => {
  return new (winston.Logger)({
    exitOnError: false,
    transports: [
      new (winston.transports.Console)(),
      new (winston.transports.File)({
        name: 'info-file',
        filename: path.join(root, logPath, 'infos.log'),
        level: 'info'
      }),
      new (winston.transports.File)({
        name: 'error-file',
        filename: path.join(root, logPath, 'errors.log'),
        level: 'error'
      })
    ]
  })
}

export const logger = createLogger(process.env.LOG_PATH)

const register = (server, options, next) => {
  server.expose('logger', logger)

  next()
}

register.attributes = {
  name: 'hapi-winston',
  version: '1.0.0'
}

export default {
  register
}
