import path from 'path'

import api from './controllers'

const endpoints = [
  // API welcome
  { method: 'GET', path: '/', config: api.root },
  // API admin
  { method: 'GET', path: '/admin/{admin?}', config: api.admin.read },
  { method: 'POST', path: '/admin', config: api.admin.create },
  { method: 'PUT', path: '/admin/{admin}', config: api.admin.update },
  { method: 'DELETE', path: '/admin/{admin}', config: api.admin.remove },
  { method: 'POST', path: '/admin/auth', config: api.admin.auth },
  // API user
  { method: 'GET', path: '/user/{user?}', config: api.user.read },
  { method: 'POST', path: '/user', config: api.user.create },
  { method: 'PUT', path: '/user/{user}', config: api.user.update },
  { method: 'POST', path: '/user/auth', config: api.user.auth },
  { method: 'POST', path: '/user/{user}/deactivate', config: api.user.deactivate },
  { method: 'POST', path: '/user/{user}/activate', config: api.user.activate },
  { method: 'POST', path: '/user/password/forgot', config: api.user.password.forgot },
  { method: 'POST', path: '/user/password/reset', config: api.user.password.reset },
  { method: 'GET', path: '/user/{user}/orders', config: api.user.orders },
  { method: 'POST', path: '/user/{user}/regen/phone', config: api.user.regen.phone },
  { method: 'POST', path: '/user/{user}/regen/email', config: api.user.regen.email },
  { method: 'DELETE', path: '/user/{user}', config: api.user.remove },
  { method: 'POST', path: '/user/payment/create', config: api.user.payment.create },
  { method: 'POST', path: '/user/payment/add', config: api.user.payment.add },
  { method: 'DELETE', path: '/user/payment', config: api.user.payment.deleteCard },
  { method: 'POST', path: '/user/payment/setMain', config: api.user.payment.setMain },
  { method: 'GET', path: '/user/payment', config: api.user.payment.list },

  // API verify
  { method: 'GET', path: '/verify/email/{token}', config: api.verify.email },
  { method: 'GET', path: '/verify/phone/{user}/{token}', config: api.verify.phone },

  // API invite
  { method: 'POST', path: '/invite', config: api.invite.invite },

  // API order
  { method: 'GET', path: '/order/{order?}', config: api.order.read },
  { method: 'POST', path: '/order', config: api.order.create },
  { method: 'PUT', path: '/order/{order}', config: api.order.update },
  { method: 'DELETE', path: '/order', config: api.order.remove },
  { method: 'GET', path: '/order/{order}/event', config: api.order.event },
  { method: 'GET', path: '/order/{order}/invoice/{format?}', config: api.order.invoice },
  { method: 'POST', path: '/order/{order}/pay', config: api.order.pay },
  { method: 'POST', path: '/order/{order}/refund', config: api.order.refund },
  { method: 'POST', path: '/order/{order}/rate', config: api.order.rate },
  { method: 'POST', path: '/order/{order}/waitForPickup', config: api.order.states.waitForPickup },
  { method: 'POST', path: '/order/{order}/inTransit', config: api.order.states.inTransit },
  { method: 'POST', path: '/order/{order}/arrived', config: api.order.states.arrived },
  { method: 'GET', path: '/order/{order}/delivered', config: api.order.states.delivered },

  // API product
  { method: 'GET', path: '/product/{product?}', config: api.product.read },
  { method: 'POST', path: '/product', config: api.product.create },
  { method: 'PUT', path: '/product/{product}', config: api.product.update },
  { method: 'DELETE', path: '/product/{product}', config: api.product.remove },
  { method: 'POST', path: '/product/calc', config: api.product.calc },

  // API hidden
  { method: 'POST', path: '/ses-bounces-and-complaints', config: api.sesBouncesComplaints },
  { method: 'POST', path: '/landing', config: api.landing.create },
  { method: 'GET', path: '/landing', config: api.landing.read },

  { method: 'POST', path: '/stripe/token', config: api.user.payment.token },

  // Only for testing (to remove)
  { method: 'GET', path: '/testing/order', config: api.testing.order },

  {
    method: 'GET',
    path: '/assets/{asset*}',
    config: {
      auth: false,
      handler: {
        directory: {
          path: path.join(__dirname, 'assets')
        }
      }
    }
  }
]

export default endpoints
