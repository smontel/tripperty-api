import Promise from 'bluebird'

export const sendPhoneValidation = (sns, user) => new Promise((resolve, reject) => {
  let Message = `${user.phone.verifyToken}`

  if (user.language === 'fr') {
    Message = `${user.phone.verifyToken}`
  }

  const sms = {
    Message,
    PhoneNumber: user.phone.international
  }

  sns.publish(sms, (err, data) => {
    if (err) {
      return reject(err)
    }

    return resolve(data)
  })
})
