import bcrypt from 'bcrypt'
import Promise from 'bluebird'
import dateFormat from 'dateformat'
import emailValidator from 'email-validator'
import GoogleAuth from 'google-auth-library'
import joi from 'joi'
import jwt from 'jsonwebtoken'
import moment from 'moment'
import request from 'request'
import uuidV4 from 'uuid/v4'
import isPlainObject from 'is-plain-object'
import unset from 'unset-value'

import config from 'auth/config'
import en from 'i18n/en.json'
import fr from 'i18n/fr.json'

const fbApiVersion = 'v2.0'

// RANDOM

// Generate a random string of nbCar characters
export const randomString = (nbCar) => Math.random().toString(36).substr(2, nbCar).toUpperCase()

// Generate a valid UUID (RFC compliant)
export const uuid = () => uuidV4()

// AUTHENTICATION

// Create signed JWT token
export const createToken = (user) => jwt.sign(user, config.key, { algorithm: 'HS256', expiresIn: 15552000000 })

// Hash passwords with a salt
export const hashPassword = (pwd, cb) => {
  bcrypt.genSalt(10, (err, salt) => {
    if (err) {
      throw err
    }

    bcrypt.hash(pwd, salt, (err, hash) => cb(err, hash))
  })
}

// Check FB connection
const ensureFBConnected = (id, token) => new Promise((resolve, reject) => {
  request.get({
    url: `https://graph.facebook.com/${fbApiVersion}/me`,
    qs: {
      access_token: token,
      fields: 'id'
    }
  }, (err, res, body) => {
    if (err || body.error) {
      return reject(err || body.error)
    }

    try {
      body = JSON.parse(body)
    } catch (err) {
      return reject(err)
    }

    return resolve(body.id === id)
  })
})

const ensureGoogleConnected = (id, token) => new Promise((resolve, reject) => {
  try {
    const auth = new GoogleAuth()
    const client = new auth.OAuth2(process.env.GOOGLE_CLIENT_ID)

    client.verifyIdToken(token, [process.env.GOOGLE_CLIENT_ID_IOS, process.env.GOOGLE_CLIENT_ID_WEB], (err) => {
      if (err) {
        return reject(err)
      }

      return resolve(true)
    })
  } catch (err) {
    return reject(err)
  }
})

// Providers checks
export const providerConnectionCheck = (provider) => {
  switch (provider) {
    case 'facebook':
      return ensureFBConnected
    case 'google':
      return ensureGoogleConnected
    default:
      return () => Promise.resolve(false)
  }
}

// OBJECT

// Merge two objects, keeping only updated properties
export const deepMergeUpdates = (original, update) => {
  if (isPlainObject(original) && isPlainObject(update)) {
    return Object.keys(update).reduce((acc, cur) => {
      if (isPlainObject(update[cur])) {
        if (isFlat(update[cur])) {
          acc[cur] = {
            ...original[cur],
            ...update[cur]
          }
        } else {
          acc[cur] = deepMergeUpdates(original[cur], update[cur])
        }
      } else {
        acc[cur] = update[cur]
      }

      return acc
    }, {})
  } else {
    throw new Error('Invalid arguments, expected two objects.')
  }
}

// Check if an expression is a flat plain object
export const isFlat = (o) => Object.keys(o).reduce((acc, cur) => {
  if (!acc || isPlainObject(o[cur])) {
    return false
  }

  return acc
}, true)

export const omit = (obj = {}, keys) => {
  const newValue = JSON.parse(JSON.stringify(obj))

  if (Array.isArray(newValue)) {
    for (let i = 0; i < newValue.length; i++) {
      newValue[i] = omit(newValue[i], keys)
    }

    return newValue
  }

  if (!isPlainObject(newValue)) {
    return newValue
  }

  if (typeof keys === 'string') {
    keys = [keys]
  }

  if (!Array.isArray(keys)) {
    return newValue
  }

  for (let j = 0; j < keys.length; j++) {
    unset(newValue, keys[j])
  }

  for (let key in newValue) {
    if (newValue.hasOwnProperty(key)) {
      newValue[key] = omit(newValue[key], keys)
    }
  }

  return newValue
}

// MISC
export const generateEventData = (order) => ({
  summary: 'Tripperty Luggage Pickup',
  description: 'Wait for our driver to pickup your luggages.',
  location: order.pickup.address,
  start: +new Date(order.pickup.date) - 1800000,
  end: +new Date(order.pickup.date) + 1800000
})

export const getFormattedDateTime = (date = new Date(), format = 'dd/mm/yy HH:MM:ss') => `${dateFormat(date, format)}`

let Joi = joi.extend({
  base: joi.date(),
  name: 'date',
  language: {
    atLeastTomorrow: 'needs to be at least tomorrow'
  },
  rules: [{
    name: 'atLeastTomorrow',
    validate (params, value, state, options) {
      const tomorrowMidnight = moment().add(1, 'days').set({ hour: 0, minute: 0, second: 0, millisecond: 0 })

      if (moment(value).isBefore(tomorrowMidnight)) {
        return this.createError('date.atLeastTomorrow', { v: value }, state, options)
      }

      return value
    }
  }]
})

Joi = Joi.extend({
  base: joi.string(),
  name: 'string',
  language: {
    email: 'is not a valid email'
  },
  rules: [{
    name: 'email',
    validate (params, value, state, options) {
      if (!emailValidator.validate(value)) {
        return this.createError('string.email', { v: value }, state, options)
      }

      return value
    }
  }]
})

export { Joi }

export const getPricing = (context, luggages, extra = 0, storage = 0, isToggledRegistrationZone = false) => {
  const pricing = {
    default: {
      ttc: context.pricing.default.ttc
    },
    total: {
      ttc: 0,
      ht: 0,
      tva: 0
    }
  }

  if (extra) {
    pricing.extraLuggage = {
      ttc: context.pricing.extraLuggage.ttc * extra
    }
  }

  if (storage > 0) {
    pricing.storage = {
      ttc: context.pricing.storage.ttc * storage * (luggages + extra)
    }
  }

  if (context.accountBalance) {
    pricing.discount = {
      ttc: context.accountBalance <= 10 ? context.accountBalance : 10
    }
  }
  if (isToggledRegistrationZone) {
    if (extra) {
      pricing.registrationZone = {
        ttc: context.pricing.registrationZone.ttc + (extra * context.pricing.registrationZoneExtra.ttc)
      }
    } else {
      pricing.registrationZone = {
        ttc: context.pricing.registrationZone.ttc
      }
    }
  }
  Object.keys(pricing).forEach((product) => {
    if (product === 'discount') {
      pricing[product].ht = pricing[product].ttc / ((100 + context.pricing.default.tva) / 100)
      pricing[product].tva = pricing[product].ttc - pricing[product].ht

      pricing.total.ttc -= pricing[product].ttc
      pricing.total.ht -= pricing[product].ht
      pricing.total.tva -= pricing[product].tva
    } else if (product !== 'total') {
      pricing[product].ht = pricing[product].ttc / ((100 + context.pricing[product].tva) / 100)
      pricing[product].tva = pricing[product].ttc - pricing[product].ht

      pricing.total.ttc += pricing[product].ttc
      pricing.total.ht += pricing[product].ht
      pricing.total.tva += pricing[product].tva
    }
  })

  Object.keys(pricing).forEach((product) => {
    pricing[product].ht = (Math.round(pricing[product].ht * 100) / 100).toString()
    pricing[product].tva = (Math.round(pricing[product].tva * 100) / 100).toString()
    pricing[product].ttc = (Math.round(pricing[product].ttc * 100) / 100).toString()
  })

  return pricing
}

export const getTranslations = (key) => ({
  en: en[key] || key,
  fr: fr[key] || key
})
